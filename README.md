Person re-identification code in C++ tested in visual studio 2017 on windows-10 64 bit machine.
We run code for VIPeR dataset.





*********************************************************************
Contents
*********************************************************************
There are 5 folders:

	1. VIPeR dataset

	2. feature extraction code

	3. Matching part

	4  Helper Function (Separate folder for each function like GOG, Normalization etc.)

	5. Reference Paper




*********************************************************************
Dataset: VIPeR: Viewpoint Invariant Pedestrian Recognition.
*********************************************************************
This dataset contains two cameras, each of which captures one image per person. 
It also provides the viewpoint angle of each image. Although it has been tested by many researchers, 
it's still one of the most challenging datasets.  
The VIPeR dataset contains 632 pedestrian image pairs taken from arbitrary viewpoints under varying illumination conditions.
Each image is scaled to 128x48 pixels.
There are two folder in dataset cam_a (used as Gallery images) and cam_b (used as Probe images) and each of folder contain 632 images.
Half of images(316)(odd numbered) is used for training and remaining (even numbered) is used for testing.



**************************************************
Training Program of Person Re-indetification.
**************************************************
We do training offline and save training parameter W (Projection matrix) and M (Mahalanobis 
matrix) in W_Projection.h and M_Mahalanobis.h header file.

Dimension:  W (249*7567) and M (249*249).

For training we use XQDA method and detail of which can be found in [1].
Here Hierarchical Gaussian Descriptor for Person Re-Identification (GOG_RGB) feature is used and
detail of which can be found in [2].

In future we will provide C++ code for training also.



*******************************************************************************
Program of Person Re-indetification, C++
********************************************************************************

Two part of Testing are: 	

1. For feature extraction code of gallery images (even numbered) ,     
2. Matching part, Find top 5 matches for given probe images from gallery images.


*******************************************************************************
Feature extraction code
********************************************************************************

File name: Main_Gallery_feature_extraction.cpp

 C++ code to extract feature for given Gallery images  and save it to text file: "Gallery_feature.txt"
 Input: Gallery images and Number of gallery images (316).
 Output: projected feature for gallery images saved in "Gallery_feature.txt" file.



Dependencies: 

1. Opencv  (For reading images)
2. GOG   (To extract GOG_RGB feature. feature dimension is: 7567)
3. Normalization (To normalize feature)
4. Projection (Project extracted feature using W matrix. Now, Projected feature dimension is: 249)		



*******************************************************************************
Testing code
********************************************************************************

File name: Main_probe_matching.cpp

Find distance between given probe image and gallery images.
Top minimum distance indicate top matches for given image.



Input: Probe image,  "Gallery_feature.txt" file extracted in program 1, Gallery images IDs.
Output: Top 5 matches for given probe image from gallery images.

This code reads a text file named Gallery_feature.txt which contains stored
features (computed by Gog_c1) of images contained in /viper/cam_a, it takes an Image id corresponding
to images saved in the folder /viper/cam_b as input (Probe image) computes its GOG feature.
It finds the least 5 distances between gallary features and input probe image feature and displays 
as top 5 matches corrosponding to probe image from all gallary images.
To find distance this code use M (Training parameter) stored in M_Mahalanobis.h header file.


Dependencies:

1. Opencv  (For reading images)
2. GOG   (To extract GOG_RGB feature. feature dimension is: 7567)
3. Normalization (To normalize feature)
4. Projection (Project extracted feature using W (Training parameter) matrix. Now, Projected feature dimension is: 249)
5. MahDist  (Find mahalanobis distance between probe and gallery images using M (Training parameter) matrix.) 
6. Sort_Array (To find index of minimum distance)

--------------------------------------------------------------------


References:

[1] S. Liao, Y. Hu, X. Zhu, and S. Z. Li. Person re-identification by local maximal occurrence representation 
and metric learning. In Proceedings of the IEEE Conference on Computer Vision and Pattern Recognition (CVPR), 
pages 2197�2206, 2015.

[2] T. Matsukawa, T. Okabe, E. Suzuki, and Y. Sato, �Hierarchical gaussian descriptor for person re-identification,� 
in Computer Vision and Pattern Recognition (CVPR), 2016.