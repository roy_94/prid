/*C++ code to extract feature for given Gallery images  and save it to text file : "Gallery_feature.txt"
Input : Gallery images and Number of gallery images(316).
	Output : projected feature for gallery images saved in "Gallery_feature.txt" file.*/


#include "opencv2\opencv.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <windows.h>

#include "GOG\GOG_C1.h"
#include "Projection\Projection_W.h"
#include "Normalization\apply_normalization22.h"


// Consatant define
#define FEATURE_SIZE 7567
#define M_SIZE 249
#define GALLERY_SIZE 316
#define IMAGE_WIDTH 48
#define IMAGE_HEIGHT 128


using namespace cv;
using namespace std;

typedef vector<string> stringvec;

// Function Declaration
void read_directory(const string& name, stringvec& Gallery_image);
unsigned char *img_mat2value(Mat img1);

int main(int argv, char **argc)
{
	string Gallery_path;
	string Gallery_path1;
	stringvec Gallery_image;
	unsigned char *p;

	double feature_vec[FEATURE_SIZE];
	double Projected_feature_vec[M_SIZE];
	double Projected_feature_Gallery[GALLERY_SIZE][M_SIZE];


	// Input gallery path
	Gallery_path = "D:\\IITB\\MTP\\Important\\Datasets\\VIPeR\\cam_a\\";
	cout << "Gallery_path is:  " << Gallery_path << endl;

	// Read all images at gallery path
	read_directory(Gallery_path, Gallery_image);
	Gallery_image.erase(Gallery_image.begin(), Gallery_image.begin() + 2);

	//create text file to store extracted feature
	ofstream myfile("Gallery_feature.txt"); // Text file to store gallery feature

	for (int i = 0; i < GALLERY_SIZE; i++)
	{
		if ((i % 50) == 0)
			cout << "evaluting feature gallery" << i << endl;
		Gallery_path1 = Gallery_path + Gallery_image[(2 * i) + 1];
		//cout << "Gallery_image       " << Gallery_path1 << endl;
		Mat img = imread(Gallery_path1, IMREAD_COLOR);
		if (img.empty())                      // Check for invalid input
		{
			cout << "Could not open or find the image" << endl;
			return -1;
		}
		p = img_mat2value(img);
		img.release();
		GOG_C1(p, feature_vec);   // Feature extraction
		apply_normalization22(feature_vec, feature_vec);  // Feature normalization
		Projection_W(feature_vec, Projected_feature_vec);  //Feature Projection

														   // Store feature in Projected_feature_Gallery
		for (int count = 0; count < M_SIZE; count++) {
			Projected_feature_Gallery[i][count] = Projected_feature_vec[count];
		};

		// Store feature in text file.
		if (myfile.is_open())
		{
			for (int count = 0; count < M_SIZE; count++) {
				myfile << Projected_feature_vec[count] << " ";
			}
			myfile << " \n ";
		}
		else cout << "Unable to open file"; 
	};
	myfile.close();

	/*for (int count = 0; count < M_SIZE; count++) {
	cout << Projected_feature_Gallery[0][count] << endl;
	};*/
	cout << "************ END **************" << endl;
	getchar();
	return 0;
}





//////////////////////// FUNCTION 1 /////////////////////////////////
///////////// Function for read directory contain  
void read_directory(const string& name, stringvec& Gallery_image)
{

	string pattern(name);
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			Gallery_image.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}



//////////////////////// FUNCTION 2 /////////////////////////////////
// Function to convert img from mat to value

unsigned char *img_mat2value(Mat img1)
{
	//cout << "Adress of mat img in function \n" << &img1 << endl;
	static unsigned char X[18432];  // 18432 = IMAGE_WIDTH*IMAGE_HEIGHT*3
	Size size(IMAGE_WIDTH, IMAGE_HEIGHT);
	resize(img1, img1, size);

	for (int j = 0; j < IMAGE_WIDTH; j++)
	{
		for (int i = 0; i < IMAGE_HEIGHT; i++)
		{
			Vec3b intensity = img1.at<Vec3b>(i, j);
			uchar red = intensity.val[0];
			uchar green = intensity.val[1];
			uchar blue = intensity.val[2];
			X[i + (j * IMAGE_HEIGHT)] = blue;
			X[i + (j * IMAGE_HEIGHT) + (IMAGE_HEIGHT * IMAGE_WIDTH)] = green;
			X[i + (j * IMAGE_HEIGHT) + (IMAGE_HEIGHT * IMAGE_WIDTH * 2)] = red;
		}
	}
	return X;
}
