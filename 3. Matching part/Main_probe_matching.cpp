/* This code reads a text file named Gallery_feature1.txt which contains stored
features (computed by Gog_c1) of images contained in /viper/cam_a, it takes an Image id corresponding
to images saved in the folder /viper/cam_b as input (Probe image) computes its GOG feature.
It finds the least 5 distances between gallery features and input probe image feature and displays
as top 5 matches corrosponding to probe image from all gallery images.
To find distance this code use M (Training parameter) stored in M.h header file.
*/


#include "opencv2\opencv.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
#include <windows.h>

#include "GOG\GOG_C1.h"
#include "Projection\Projection_W.h"
#include "Normalization\apply_normalization22.h"
#include "MahDist\MahDist_C.h"
#include "Sort_Array\Sort_array.h"


// Consatant define
#define FEATURE_SIZE 7567
#define M_SIZE 249
#define GALLERY_SIZE 316
#define IMAGE_WIDTH 48
#define IMAGE_HEIGHT 128


using namespace cv;
using namespace std;

typedef vector<string> stringvec;

// Function Declaration
void read_directory(const string& name, stringvec& Gallery_image);
void ShowManyImages(string title, int nArgs, ...);
unsigned char *img_mat2value(Mat img1);

int main(int argv, char **argc)
{
	// Variable declaration
	static unsigned char *p;
	string Gallery_path;
	string Gallery_path1;
	stringvec Gallery_image;
	string Probe_path;
	string Probe_path1;
	stringvec Probe_image;
	int Gallery_size = 0;



	double feature_vec[FEATURE_SIZE];
	double Projected_feature_vec[M_SIZE];
	double Projected_feature_probe[M_SIZE];
	double Projected_feature_Gallery[GALLERY_SIZE][M_SIZE];

	double dist[GALLERY_SIZE];
	double  Sorted_index[GALLERY_SIZE];;



	Gallery_path = "D:\\IITB\\MTP\\Important\\Datasets\\VIPeR\\cam_a\\";


	//*********** Read gallery Images name in  Gallery_image string vector ************* //

	read_directory(Gallery_path, Gallery_image);
	Gallery_image.erase(Gallery_image.begin(), Gallery_image.begin() + 2);

	//*************************************************//


	//*********** load feature of gallery Images saved in text file "Gallery_feature1.txt"************* //
	// open file for reading
	ifstream inFile;
	inFile.open("Gallery_feature.txt");


	// checks if file opened
	if (inFile.fail()) {
		cout << "error loading .txt file reading" << endl;
		return 1;
	}
	// Extract saved feature in array Projected_feature_Gallery
	int n1 = 0;
	while (!inFile.eof()) {
		for (int i = 0; i<M_SIZE; i++) {
			inFile >> Projected_feature_Gallery[n1][i];
		}
		++n1;
	}


	//*************************************************//

	//************* Find distance between prob and galaary images ************
	// Read probe image from user
	Probe_path = "D:\\IITB\\MTP\\Important\\Datasets\\VIPeR\\cam_b\\";
	read_directory(Probe_path, Probe_image);
	Probe_image.erase(Probe_image.begin(), Probe_image.begin() + 2);
	int probe_no;
	cout << "enter probe image number (1-316): ";
	cin >> probe_no;
	Probe_path1 = Probe_path + Probe_image[(probe_no * 2) + 1];
	cout << "Probe_path:" << Probe_path1 << endl;

	Mat img = imread(Probe_path1, IMREAD_COLOR);
	if (img.empty())                      // Check for invalid input
	{
		cout << "Could not open or find the image" << endl;
		return -1;
	}
	p = img_mat2value(img);

	// Find GOG feature for given probe image
	GOG_C1(p, feature_vec);
	apply_normalization22(feature_vec, feature_vec);    //Feature normalization
	Projection_W(feature_vec, Projected_feature_probe); // Feature projection using training parameter W

														//Find distance between Gallery images and probe image
	for (int i = 0; i < GALLERY_SIZE; i++)
	{
		dist[i] = MahDist_C(Projected_feature_Gallery[i], Projected_feature_probe);
		//cout << "distance: " << dist << endl;
	}

	// Sort distance and give index of sorted dist in asecndnig order 
	Sort_array(dist, Sorted_index);



	// ********************************************************************* //
	// Display top 5 matches from gallery images to given probe image.
	Mat img_array[5];
	Gallery_path1 = Gallery_path + Gallery_image[(Sorted_index[0] * 2) + 1];
	img_array[0] = imread(Gallery_path1, IMREAD_COLOR);
	Gallery_path1 = Gallery_path + Gallery_image[(Sorted_index[1] * 2) + 1];
	img_array[1] = imread(Gallery_path1, IMREAD_COLOR);
	Gallery_path1 = Gallery_path + Gallery_image[(Sorted_index[2] * 2) + 1];
	img_array[2] = imread(Gallery_path1, IMREAD_COLOR);
	Gallery_path1 = Gallery_path + Gallery_image[(Sorted_index[3] * 2) + 1];
	img_array[3] = imread(Gallery_path1, IMREAD_COLOR);
	Gallery_path1 = Gallery_path + Gallery_image[(Sorted_index[4] * 2) + 1];
	img_array[4] = imread(Gallery_path1, IMREAD_COLOR);

	//show all image in one window
	ShowManyImages("Image", 6, img, img_array[0], img_array[1], img_array[2], img_array[3], img_array[4]);
	
	waitKey(0);
	getchar();
	return 0;

}





//////////////////////// FUNCTION 1 /////////////////////////////////
///////////// Function for read directory contain  
void read_directory(const string& name, stringvec& Gallery_image)
{

	string pattern(name);
	pattern.append("\\*");
	WIN32_FIND_DATA data;
	HANDLE hFind;
	if ((hFind = FindFirstFile(pattern.c_str(), &data)) != INVALID_HANDLE_VALUE) {
		do {
			Gallery_image.push_back(data.cFileName);
		} while (FindNextFile(hFind, &data) != 0);
		FindClose(hFind);
	}
}



//////////////////////// FUNCTION 2 /////////////////////////////////
// Function to convert img from mat to value

unsigned char *img_mat2value(Mat img1)
{
	//cout << "Adress of mat img in function \n" << &img1 << endl;
	static unsigned char X[18432];  // 18432 = IMAGE_WIDTH*IMAGE_HEIGHT*3
	Size size(IMAGE_WIDTH, IMAGE_HEIGHT);
	resize(img1, img1, size);

	for (int j = 0; j < IMAGE_WIDTH; j++)
	{
		for (int i = 0; i < IMAGE_HEIGHT; i++)
		{
			Vec3b intensity = img1.at<Vec3b>(i, j);
			uchar red = intensity.val[0];
			uchar green = intensity.val[1];
			uchar blue = intensity.val[2];
			X[i + (j * IMAGE_HEIGHT)] = blue;
			X[i + (j * IMAGE_HEIGHT) + (IMAGE_HEIGHT * IMAGE_WIDTH)] = green;
			X[i + (j * IMAGE_HEIGHT) + (IMAGE_HEIGHT * IMAGE_WIDTH * 2)] = red;
		}
	}
	return X;
}




//////////////////////////////// Function 3  ///////////////////////////
// Function for displaying multiple images
void ShowManyImages(string title, int nArgs, ...) {
	int size;
	int i;
	int m, n;
	int x, y;

	// w - Maximum number of images in a row
	// h - Maximum number of images in a column
	int w, h;

	// scale - How much we have to resize the image
	float scale;
	int max;

	// If the number of arguments is lesser than 0 or greater than 12
	// return without displaying
	if (nArgs <= 0) {
		printf("Number of arguments too small....\n");
		return;
	}
	else if (nArgs > 14) {
		printf("Number of arguments too large, can only handle maximally 12 images at a time ...\n");
		return;
	}
	// Determine the size of the image,
	// and the number of rows/cols
	// from number of arguments
	else if (nArgs == 1 || nArgs == 2 || nArgs == 3 || nArgs == 4) {
		w = nArgs, h = 1;
		size = 300;
	}
	else if (nArgs == 5 || nArgs == 6 || nArgs == 7 || nArgs == 8) {
		w = nArgs; h = 1;
		size = 200;
	}
	else {
		w = nArgs; h = 1;
		size = 150;
	}

	// Create a new 3 channel image
	Mat DispImage = Mat::zeros(Size(100 + size * w, 60 + size * h), CV_8UC3);

	// Used to get the arguments passed
	va_list args;
	va_start(args, nArgs);

	// Loop for nArgs number of arguments
	for (i = 0, m = 20, n = 20; i < nArgs; i++, m += (0 + size)) {
		// Get the Pointer to the IplImage
		Mat img = va_arg(args, Mat);

		// Check whether it is NULL or not
		// If it is NULL, release the image, and return
		if (img.empty()) {
			printf("Invalid arguments");
			return;
		}

		// Find the width and height of the image
		x = img.cols;
		y = img.rows;

		// Find whether height or width is greater in order to resize the image
		max = (x > y) ? x : y;

		// Find the scaling factor to resize the image
		scale = (float)((float)max / size);

		// Used to Align the images
		if (i % w == 0 && m != 20) {
			m = 20;
			n += 20 + size;
		}

		// Set the image ROI to display the current image
		// Resize the input image and copy the it to the Single Big Image
		Rect ROI(m, n, (int)(x / scale), (int)(y / scale));
		Mat temp; resize(img, temp, Size(ROI.width, ROI.height));
		temp.copyTo(DispImage(ROI));
	}

	// Create a new window, and show the Single Big Image
	namedWindow(title, 1);
	imshow(title, DispImage);
	waitKey();

	// End the number of arguments
	va_end(args);
}
