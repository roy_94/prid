//
// File: convn.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "convn.h"

// Function Definitions

//
// Arguments    : const double A[6240]
//                const double B[3]
//                double C[6240]
// Return Type  : void
//
void b_convn(const double A[6240], const double B[3], double C[6240])
{
  int k;
  int iC;
  int c;
  int iB;
  int i;
  int firstRowA;
  int b_i;
  int a_length;
  int cidx;
  int r;
  memset(&C[0], 0, 6240U * sizeof(double));
  for (k = 0; k < 48; k++) {
    iC = k * 130;
    c = k * 130;
    iB = 0;
    for (i = 0; i < 3; i++) {
      firstRowA = (i < 1);
      if (i + 130 <= 130) {
        b_i = 130;
      } else {
        b_i = 131 - i;
      }

      a_length = b_i - firstRowA;
      firstRowA += c;
      cidx = iC;
      for (r = 1; r <= a_length; r++) {
        C[cidx] += B[iB] * A[firstRowA];
        firstRowA++;
        cidx++;
      }

      iB++;
      if (i >= 1) {
        iC++;
      }
    }
  }
}

//
// Arguments    : const double A[6400]
//                const double B[3]
//                double C[6400]
// Return Type  : void
//
void convn(const double A[6400], const double B[3], double C[6400])
{
  int j;
  int lastColA;
  int k;
  int aidx;
  int b_j;
  int cidx;
  int r;
  memset(&C[0], 0, 6400U * sizeof(double));
  for (j = 0; j < 3; j++) {
    if (j + 49 < 50) {
      lastColA = 49;
    } else {
      lastColA = 50 - j;
    }

    for (k = (j < 1); k <= lastColA; k++) {
      aidx = k << 7;
      if (j + k > 1) {
        b_j = (j + k) - 1;
      } else {
        b_j = 0;
      }

      cidx = b_j << 7;
      for (r = 0; r < 128; r++) {
        C[cidx] += B[j] * A[aidx];
        aidx++;
        cidx++;
      }
    }
  }
}

//
// File trailer for convn.cpp
//
// [EOF]
//
