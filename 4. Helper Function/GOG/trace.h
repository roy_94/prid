//
// File: trace.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef TRACE_H
#define TRACE_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern double b_trace(const double a[2025]);
extern double trace(const double a[64]);

#endif

//
// File trailer for trace.h
//
// [EOF]
//
