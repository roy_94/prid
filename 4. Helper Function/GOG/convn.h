//
// File: convn.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef CONVN_H
#define CONVN_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_convn(const double A[6240], const double B[3], double C[6240]);
extern void convn(const double A[6400], const double B[3], double C[6400]);

#endif

//
// File trailer for convn.h
//
// [EOF]
//
