//
// File: mod.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "mod.h"
#include "GOG_C1_rtwutil.h"

// Function Definitions

//
// Arguments    : double x
//                double y
// Return Type  : double
//
double b_mod(double x, double y)
{
  double r;
  if (y == floor(y)) {
    r = x - floor(x / y) * y;
  } else {
    r = x / y;
    if (fabs(r - rt_roundd_snf(r)) <= 2.2204460492503131E-16 * fabs(r)) {
      r = 0.0;
    } else {
      r = (r - floor(r)) * y;
    }
  }

  return r;
}

//
// File trailer for mod.cpp
//
// [EOF]
//
