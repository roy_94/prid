//
// File: svd.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "svd.h"
#include "xaxpy.h"
#include "xdotc.h"
#include "xnrm2.h"
#include "xscal.h"
#include "xrot.h"
#include "xrotg.h"
#include "xswap.h"

// Function Definitions

//
// Arguments    : const double A[2116]
//                double U[2116]
//                double S[2116]
// Return Type  : void
//
void b_svd(const double A[2116], double U[2116], double S[2116])
{
  double b_A[2116];
  double s[46];
  double e[46];
  double work[46];
  int q;
  int m;
  int qs;
  boolean_T apply_transform;
  double ztest0;
  int kase;
  int qjj;
  int iter;
  double ztest;
  double snorm;
  double rt;
  int exitg3;
  boolean_T exitg2;
  double f;
  double varargin_1[5];
  double mtmp;
  boolean_T exitg1;
  double sqds;
  memcpy(&b_A[0], &A[0], 2116U * sizeof(double));
  memset(&s[0], 0, 46U * sizeof(double));
  memset(&e[0], 0, 46U * sizeof(double));
  memset(&work[0], 0, 46U * sizeof(double));
  memset(&U[0], 0, 2116U * sizeof(double));
  for (q = 0; q < 45; q++) {
    qs = q + 46 * q;
    apply_transform = false;
    ztest0 = c_xnrm2(46 - q, b_A, qs + 1);
    if (ztest0 > 0.0) {
      apply_transform = true;
      if (b_A[qs] < 0.0) {
        s[q] = -ztest0;
      } else {
        s[q] = ztest0;
      }

      if (fabs(s[q]) >= 1.0020841800044864E-292) {
        ztest0 = 1.0 / s[q];
        kase = (qs - q) + 46;
        for (qjj = qs; qjj + 1 <= kase; qjj++) {
          b_A[qjj] *= ztest0;
        }
      } else {
        kase = (qs - q) + 46;
        for (qjj = qs; qjj + 1 <= kase; qjj++) {
          b_A[qjj] /= s[q];
        }
      }

      b_A[qs]++;
      s[q] = -s[q];
    } else {
      s[q] = 0.0;
    }

    for (kase = q + 1; kase + 1 < 47; kase++) {
      qjj = q + 46 * kase;
      if (apply_transform) {
        d_xaxpy(46 - q, -(b_xdotc(46 - q, b_A, qs + 1, b_A, qjj + 1) / b_A[q +
                          46 * q]), qs + 1, b_A, qjj + 1);
      }

      e[kase] = b_A[qjj];
    }

    for (qjj = q; qjj + 1 < 47; qjj++) {
      U[qjj + 46 * q] = b_A[qjj + 46 * q];
    }

    if (q + 1 <= 44) {
      ztest0 = d_xnrm2(45 - q, e, q + 2);
      if (ztest0 == 0.0) {
        e[q] = 0.0;
      } else {
        if (e[q + 1] < 0.0) {
          e[q] = -ztest0;
        } else {
          e[q] = ztest0;
        }

        ztest0 = e[q];
        if (fabs(e[q]) >= 1.0020841800044864E-292) {
          ztest0 = 1.0 / e[q];
          for (qjj = q + 1; qjj + 1 < 47; qjj++) {
            e[qjj] *= ztest0;
          }
        } else {
          for (qjj = q + 1; qjj + 1 < 47; qjj++) {
            e[qjj] /= ztest0;
          }
        }

        e[q + 1]++;
        e[q] = -e[q];
        for (qjj = q + 1; qjj + 1 < 47; qjj++) {
          work[qjj] = 0.0;
        }

        for (kase = q + 1; kase + 1 < 47; kase++) {
          e_xaxpy(45 - q, e[kase], b_A, (q + 46 * kase) + 2, work, q + 2);
        }

        for (kase = q + 1; kase + 1 < 47; kase++) {
          f_xaxpy(45 - q, -e[kase] / e[q + 1], work, q + 2, b_A, (q + 46 * kase)
                  + 2);
        }
      }
    }
  }

  m = 44;
  s[45] = b_A[2115];
  e[44] = b_A[2114];
  e[45] = 0.0;
  memset(&U[2070], 0, 46U * sizeof(double));
  U[2115] = 1.0;
  for (q = 44; q >= 0; q += -1) {
    qs = q + 46 * q;
    if (s[q] != 0.0) {
      for (kase = q + 1; kase + 1 < 47; kase++) {
        qjj = (q + 46 * kase) + 1;
        d_xaxpy(46 - q, -(b_xdotc(46 - q, U, qs + 1, U, qjj) / U[qs]), qs + 1, U,
                qjj);
      }

      for (qjj = q; qjj + 1 < 47; qjj++) {
        U[qjj + 46 * q] = -U[qjj + 46 * q];
      }

      U[qs]++;
      for (qjj = 1; qjj <= q; qjj++) {
        U[(qjj + 46 * q) - 1] = 0.0;
      }
    } else {
      memset(&U[q * 46], 0, 46U * sizeof(double));
      U[qs] = 1.0;
    }
  }

  for (q = 0; q < 46; q++) {
    ztest = e[q];
    if (s[q] != 0.0) {
      rt = fabs(s[q]);
      ztest0 = s[q] / rt;
      s[q] = rt;
      if (q + 1 < 46) {
        ztest = e[q] / ztest0;
      }

      b_xscal(ztest0, U, 1 + 46 * q);
    }

    if ((q + 1 < 46) && (ztest != 0.0)) {
      rt = fabs(ztest);
      ztest0 = ztest;
      ztest = rt;
      s[q + 1] *= rt / ztest0;
    }

    e[q] = ztest;
  }

  iter = 0;
  snorm = 0.0;
  for (qjj = 0; qjj < 46; qjj++) {
    ztest0 = fabs(s[qjj]);
    ztest = fabs(e[qjj]);
    if ((ztest0 >= ztest) || rtIsNaN(ztest)) {
    } else {
      ztest0 = ztest;
    }

    if ((snorm >= ztest0) || rtIsNaN(ztest0)) {
    } else {
      snorm = ztest0;
    }
  }

  while ((m + 2 > 0) && (!(iter >= 75))) {
    qjj = m;
    do {
      exitg3 = 0;
      q = qjj + 1;
      if (qjj + 1 == 0) {
        exitg3 = 1;
      } else {
        ztest0 = fabs(e[qjj]);
        if ((ztest0 <= 2.2204460492503131E-16 * (fabs(s[qjj]) + fabs(s[qjj + 1])))
            || (ztest0 <= 1.0020841800044864E-292) || ((iter > 20) && (ztest0 <=
              2.2204460492503131E-16 * snorm))) {
          e[qjj] = 0.0;
          exitg3 = 1;
        } else {
          qjj--;
        }
      }
    } while (exitg3 == 0);

    if (qjj + 1 == m + 1) {
      kase = 4;
    } else {
      qs = m + 2;
      kase = m + 2;
      exitg2 = false;
      while ((!exitg2) && (kase >= qjj + 1)) {
        qs = kase;
        if (kase == qjj + 1) {
          exitg2 = true;
        } else {
          ztest0 = 0.0;
          if (kase < m + 2) {
            ztest0 = fabs(e[kase - 1]);
          }

          if (kase > qjj + 2) {
            ztest0 += fabs(e[kase - 2]);
          }

          ztest = fabs(s[kase - 1]);
          if ((ztest <= 2.2204460492503131E-16 * ztest0) || (ztest <=
               1.0020841800044864E-292)) {
            s[kase - 1] = 0.0;
            exitg2 = true;
          } else {
            kase--;
          }
        }
      }

      if (qs == qjj + 1) {
        kase = 3;
      } else if (qs == m + 2) {
        kase = 1;
      } else {
        kase = 2;
        q = qs;
      }
    }

    switch (kase) {
     case 1:
      f = e[m];
      e[m] = 0.0;
      for (qjj = m - 1; qjj + 2 >= q + 1; qjj--) {
        xrotg(&s[qjj + 1], &f, &ztest0, &ztest);
        if (qjj + 2 > q + 1) {
          f = -ztest * e[qjj];
          e[qjj] *= ztest0;
        }
      }
      break;

     case 2:
      f = e[q - 1];
      e[q - 1] = 0.0;
      for (qjj = q; qjj + 1 <= m + 2; qjj++) {
        xrotg(&s[qjj], &f, &ztest0, &ztest);
        f = -ztest * e[qjj];
        e[qjj] *= ztest0;
        b_xrot(U, 1 + 46 * qjj, 1 + 46 * (q - 1), ztest0, ztest);
      }
      break;

     case 3:
      varargin_1[0] = fabs(s[m + 1]);
      varargin_1[1] = fabs(s[m]);
      varargin_1[2] = fabs(e[m]);
      varargin_1[3] = fabs(s[q]);
      varargin_1[4] = fabs(e[q]);
      kase = 1;
      mtmp = varargin_1[0];
      if (rtIsNaN(varargin_1[0])) {
        qjj = 2;
        exitg1 = false;
        while ((!exitg1) && (qjj < 6)) {
          kase = qjj;
          if (!rtIsNaN(varargin_1[qjj - 1])) {
            mtmp = varargin_1[qjj - 1];
            exitg1 = true;
          } else {
            qjj++;
          }
        }
      }

      if (kase < 5) {
        while (kase + 1 < 6) {
          if (varargin_1[kase] > mtmp) {
            mtmp = varargin_1[kase];
          }

          kase++;
        }
      }

      f = s[m + 1] / mtmp;
      ztest0 = s[m] / mtmp;
      ztest = e[m] / mtmp;
      sqds = s[q] / mtmp;
      rt = ((ztest0 + f) * (ztest0 - f) + ztest * ztest) / 2.0;
      ztest0 = f * ztest;
      ztest0 *= ztest0;
      if ((rt != 0.0) || (ztest0 != 0.0)) {
        ztest = sqrt(rt * rt + ztest0);
        if (rt < 0.0) {
          ztest = -ztest;
        }

        ztest = ztest0 / (rt + ztest);
      } else {
        ztest = 0.0;
      }

      f = (sqds + f) * (sqds - f) + ztest;
      rt = sqds * (e[q] / mtmp);
      for (qjj = q + 1; qjj <= m + 1; qjj++) {
        xrotg(&f, &rt, &ztest0, &ztest);
        if (qjj > q + 1) {
          e[qjj - 2] = f;
        }

        f = ztest0 * s[qjj - 1] + ztest * e[qjj - 1];
        e[qjj - 1] = ztest0 * e[qjj - 1] - ztest * s[qjj - 1];
        rt = ztest * s[qjj];
        s[qjj] *= ztest0;
        s[qjj - 1] = f;
        xrotg(&s[qjj - 1], &rt, &ztest0, &ztest);
        f = ztest0 * e[qjj - 1] + ztest * s[qjj];
        s[qjj] = -ztest * e[qjj - 1] + ztest0 * s[qjj];
        rt = ztest * e[qjj];
        e[qjj] *= ztest0;
        b_xrot(U, 1 + 46 * (qjj - 1), 1 + 46 * qjj, ztest0, ztest);
      }

      e[m] = f;
      iter++;
      break;

     default:
      if (s[q] < 0.0) {
        s[q] = -s[q];
      }

      kase = q + 1;
      while ((q + 1 < 46) && (s[q] < s[kase])) {
        rt = s[q];
        s[q] = s[kase];
        s[kase] = rt;
        c_xswap(U, 1 + 46 * q, 1 + 46 * (q + 1));
        q = kase;
        kase++;
      }

      iter = 0;
      m--;
      break;
    }
  }

  memcpy(&e[0], &s[0], 46U * sizeof(double));
  memset(&S[0], 0, 2116U * sizeof(double));
  for (qjj = 0; qjj < 46; qjj++) {
    S[qjj + 46 * qjj] = e[qjj];
  }
}

//
// Arguments    : const double A[81]
//                double U[81]
//                double S[81]
// Return Type  : void
//
void svd(const double A[81], double U[81], double S[81])
{
  double b_A[81];
  double s[9];
  double e[9];
  double work[9];
  int q;
  int m;
  int qs;
  boolean_T apply_transform;
  double ztest0;
  int kase;
  int qjj;
  int iter;
  double ztest;
  double snorm;
  double rt;
  int exitg3;
  boolean_T exitg2;
  double f;
  double varargin_1[5];
  double mtmp;
  boolean_T exitg1;
  double sqds;
  memcpy(&b_A[0], &A[0], 81U * sizeof(double));
  memset(&s[0], 0, 9U * sizeof(double));
  memset(&e[0], 0, 9U * sizeof(double));
  memset(&work[0], 0, 9U * sizeof(double));
  memset(&U[0], 0, 81U * sizeof(double));
  for (q = 0; q < 8; q++) {
    qs = q + 9 * q;
    apply_transform = false;
    ztest0 = xnrm2(9 - q, b_A, qs + 1);
    if (ztest0 > 0.0) {
      apply_transform = true;
      if (b_A[qs] < 0.0) {
        s[q] = -ztest0;
      } else {
        s[q] = ztest0;
      }

      if (fabs(s[q]) >= 1.0020841800044864E-292) {
        ztest0 = 1.0 / s[q];
        kase = (qs - q) + 9;
        for (qjj = qs; qjj + 1 <= kase; qjj++) {
          b_A[qjj] *= ztest0;
        }
      } else {
        kase = (qs - q) + 9;
        for (qjj = qs; qjj + 1 <= kase; qjj++) {
          b_A[qjj] /= s[q];
        }
      }

      b_A[qs]++;
      s[q] = -s[q];
    } else {
      s[q] = 0.0;
    }

    for (kase = q + 1; kase + 1 < 10; kase++) {
      qjj = q + 9 * kase;
      if (apply_transform) {
        xaxpy(9 - q, -(xdotc(9 - q, b_A, qs + 1, b_A, qjj + 1) / b_A[q + 9 * q]),
              qs + 1, b_A, qjj + 1);
      }

      e[kase] = b_A[qjj];
    }

    for (qjj = q; qjj + 1 < 10; qjj++) {
      U[qjj + 9 * q] = b_A[qjj + 9 * q];
    }

    if (q + 1 <= 7) {
      ztest0 = b_xnrm2(8 - q, e, q + 2);
      if (ztest0 == 0.0) {
        e[q] = 0.0;
      } else {
        if (e[q + 1] < 0.0) {
          e[q] = -ztest0;
        } else {
          e[q] = ztest0;
        }

        ztest0 = e[q];
        if (fabs(e[q]) >= 1.0020841800044864E-292) {
          ztest0 = 1.0 / e[q];
          for (qjj = q + 1; qjj + 1 < 10; qjj++) {
            e[qjj] *= ztest0;
          }
        } else {
          for (qjj = q + 1; qjj + 1 < 10; qjj++) {
            e[qjj] /= ztest0;
          }
        }

        e[q + 1]++;
        e[q] = -e[q];
        for (qjj = q + 1; qjj + 1 < 10; qjj++) {
          work[qjj] = 0.0;
        }

        for (kase = q + 1; kase + 1 < 10; kase++) {
          b_xaxpy(8 - q, e[kase], b_A, (q + 9 * kase) + 2, work, q + 2);
        }

        for (kase = q + 1; kase + 1 < 10; kase++) {
          c_xaxpy(8 - q, -e[kase] / e[q + 1], work, q + 2, b_A, (q + 9 * kase) +
                  2);
        }
      }
    }
  }

  m = 7;
  s[8] = b_A[80];
  e[7] = b_A[79];
  e[8] = 0.0;
  memset(&U[72], 0, 9U * sizeof(double));
  U[80] = 1.0;
  for (q = 7; q >= 0; q += -1) {
    qs = q + 9 * q;
    if (s[q] != 0.0) {
      for (kase = q + 1; kase + 1 < 10; kase++) {
        qjj = (q + 9 * kase) + 1;
        xaxpy(9 - q, -(xdotc(9 - q, U, qs + 1, U, qjj) / U[qs]), qs + 1, U, qjj);
      }

      for (qjj = q; qjj + 1 < 10; qjj++) {
        U[qjj + 9 * q] = -U[qjj + 9 * q];
      }

      U[qs]++;
      for (qjj = 1; qjj <= q; qjj++) {
        U[(qjj + 9 * q) - 1] = 0.0;
      }
    } else {
      memset(&U[q * 9], 0, 9U * sizeof(double));
      U[qs] = 1.0;
    }
  }

  for (q = 0; q < 9; q++) {
    ztest = e[q];
    if (s[q] != 0.0) {
      rt = fabs(s[q]);
      ztest0 = s[q] / rt;
      s[q] = rt;
      if (q + 1 < 9) {
        ztest = e[q] / ztest0;
      }

      xscal(ztest0, U, 1 + 9 * q);
    }

    if ((q + 1 < 9) && (ztest != 0.0)) {
      rt = fabs(ztest);
      ztest0 = ztest;
      ztest = rt;
      s[q + 1] *= rt / ztest0;
    }

    e[q] = ztest;
  }

  iter = 0;
  snorm = 0.0;
  for (qjj = 0; qjj < 9; qjj++) {
    ztest0 = fabs(s[qjj]);
    ztest = fabs(e[qjj]);
    if ((ztest0 >= ztest) || rtIsNaN(ztest)) {
    } else {
      ztest0 = ztest;
    }

    if ((snorm >= ztest0) || rtIsNaN(ztest0)) {
    } else {
      snorm = ztest0;
    }
  }

  while ((m + 2 > 0) && (!(iter >= 75))) {
    qjj = m;
    do {
      exitg3 = 0;
      q = qjj + 1;
      if (qjj + 1 == 0) {
        exitg3 = 1;
      } else {
        ztest0 = fabs(e[qjj]);
        if ((ztest0 <= 2.2204460492503131E-16 * (fabs(s[qjj]) + fabs(s[qjj + 1])))
            || (ztest0 <= 1.0020841800044864E-292) || ((iter > 20) && (ztest0 <=
              2.2204460492503131E-16 * snorm))) {
          e[qjj] = 0.0;
          exitg3 = 1;
        } else {
          qjj--;
        }
      }
    } while (exitg3 == 0);

    if (qjj + 1 == m + 1) {
      kase = 4;
    } else {
      qs = m + 2;
      kase = m + 2;
      exitg2 = false;
      while ((!exitg2) && (kase >= qjj + 1)) {
        qs = kase;
        if (kase == qjj + 1) {
          exitg2 = true;
        } else {
          ztest0 = 0.0;
          if (kase < m + 2) {
            ztest0 = fabs(e[kase - 1]);
          }

          if (kase > qjj + 2) {
            ztest0 += fabs(e[kase - 2]);
          }

          ztest = fabs(s[kase - 1]);
          if ((ztest <= 2.2204460492503131E-16 * ztest0) || (ztest <=
               1.0020841800044864E-292)) {
            s[kase - 1] = 0.0;
            exitg2 = true;
          } else {
            kase--;
          }
        }
      }

      if (qs == qjj + 1) {
        kase = 3;
      } else if (qs == m + 2) {
        kase = 1;
      } else {
        kase = 2;
        q = qs;
      }
    }

    switch (kase) {
     case 1:
      f = e[m];
      e[m] = 0.0;
      for (qjj = m - 1; qjj + 2 >= q + 1; qjj--) {
        xrotg(&s[qjj + 1], &f, &ztest0, &ztest);
        if (qjj + 2 > q + 1) {
          f = -ztest * e[qjj];
          e[qjj] *= ztest0;
        }
      }
      break;

     case 2:
      f = e[q - 1];
      e[q - 1] = 0.0;
      for (qjj = q; qjj + 1 <= m + 2; qjj++) {
        xrotg(&s[qjj], &f, &ztest0, &ztest);
        f = -ztest * e[qjj];
        e[qjj] *= ztest0;
        xrot(U, 1 + 9 * qjj, 1 + 9 * (q - 1), ztest0, ztest);
      }
      break;

     case 3:
      varargin_1[0] = fabs(s[m + 1]);
      varargin_1[1] = fabs(s[m]);
      varargin_1[2] = fabs(e[m]);
      varargin_1[3] = fabs(s[q]);
      varargin_1[4] = fabs(e[q]);
      kase = 1;
      mtmp = varargin_1[0];
      if (rtIsNaN(varargin_1[0])) {
        qjj = 2;
        exitg1 = false;
        while ((!exitg1) && (qjj < 6)) {
          kase = qjj;
          if (!rtIsNaN(varargin_1[qjj - 1])) {
            mtmp = varargin_1[qjj - 1];
            exitg1 = true;
          } else {
            qjj++;
          }
        }
      }

      if (kase < 5) {
        while (kase + 1 < 6) {
          if (varargin_1[kase] > mtmp) {
            mtmp = varargin_1[kase];
          }

          kase++;
        }
      }

      f = s[m + 1] / mtmp;
      ztest0 = s[m] / mtmp;
      ztest = e[m] / mtmp;
      sqds = s[q] / mtmp;
      rt = ((ztest0 + f) * (ztest0 - f) + ztest * ztest) / 2.0;
      ztest0 = f * ztest;
      ztest0 *= ztest0;
      if ((rt != 0.0) || (ztest0 != 0.0)) {
        ztest = sqrt(rt * rt + ztest0);
        if (rt < 0.0) {
          ztest = -ztest;
        }

        ztest = ztest0 / (rt + ztest);
      } else {
        ztest = 0.0;
      }

      f = (sqds + f) * (sqds - f) + ztest;
      rt = sqds * (e[q] / mtmp);
      for (qjj = q + 1; qjj <= m + 1; qjj++) {
        xrotg(&f, &rt, &ztest0, &ztest);
        if (qjj > q + 1) {
          e[qjj - 2] = f;
        }

        f = ztest0 * s[qjj - 1] + ztest * e[qjj - 1];
        e[qjj - 1] = ztest0 * e[qjj - 1] - ztest * s[qjj - 1];
        rt = ztest * s[qjj];
        s[qjj] *= ztest0;
        s[qjj - 1] = f;
        xrotg(&s[qjj - 1], &rt, &ztest0, &ztest);
        f = ztest0 * e[qjj - 1] + ztest * s[qjj];
        s[qjj] = -ztest * e[qjj - 1] + ztest0 * s[qjj];
        rt = ztest * e[qjj];
        e[qjj] *= ztest0;
        xrot(U, 1 + 9 * (qjj - 1), 1 + 9 * qjj, ztest0, ztest);
      }

      e[m] = f;
      iter++;
      break;

     default:
      if (s[q] < 0.0) {
        s[q] = -s[q];
      }

      kase = q + 1;
      while ((q + 1 < 9) && (s[q] < s[kase])) {
        rt = s[q];
        s[q] = s[kase];
        s[kase] = rt;
        xswap(U, 1 + 9 * q, 1 + 9 * (q + 1));
        q = kase;
        kase++;
      }

      iter = 0;
      m--;
      break;
    }
  }

  memcpy(&e[0], &s[0], 9U * sizeof(double));
  memset(&S[0], 0, 81U * sizeof(double));
  for (qjj = 0; qjj < 9; qjj++) {
    S[qjj + 9 * qjj] = e[qjj];
  }
}

//
// File trailer for svd.cpp
//
// [EOF]
//
