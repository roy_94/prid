//
// File: svd.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef SVD_H
#define SVD_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_svd(const double A[2116], double U[2116], double S[2116]);
extern void svd(const double A[81], double U[81], double S[81]);

#endif

//
// File trailer for svd.h
//
// [EOF]
//
