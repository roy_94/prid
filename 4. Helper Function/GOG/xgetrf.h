//
// File: xgetrf.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XGETRF_H
#define XGETRF_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void xgetrf(double A[2025], int ipiv[45]);

#endif

//
// File trailer for xgetrf.h
//
// [EOF]
//
