//
// File: abs.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef ABS_H
#define ABS_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_abs(const double x[768], double y[768]);
extern void c_abs(const double x[288], double y[288]);

#endif

//
// File trailer for abs.h
//
// [EOF]
//
