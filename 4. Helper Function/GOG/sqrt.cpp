//
// File: sqrt.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "sqrt.h"

// Function Definitions

//
// Arguments    : double x[6144]
// Return Type  : void
//
void b_sqrt(double x[6144])
{
  int k;
  for (k = 0; k < 6144; k++) {
    x[k] = sqrt(x[k]);
  }
}

//
// File trailer for sqrt.cpp
//
// [EOF]
//
