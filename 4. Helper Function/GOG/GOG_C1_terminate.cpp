//
// File: GOG_C1_terminate.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "GOG_C1_terminate.h"
#include "GOG_C1_data.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void GOG_C1_terminate()
{
  omp_destroy_nest_lock(&emlrtNestLockGlobal);
}

//
// File trailer for GOG_C1_terminate.cpp
//
// [EOF]
//
