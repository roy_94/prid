//
// File: sum.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "sum.h"

// Function Definitions

//
// Arguments    : const double x[288]
//                double y[48]
// Return Type  : void
//
void b_sum(const double x[288], double y[48])
{
  int j;
  double s;
  int k;
  for (j = 0; j < 48; j++) {
    s = x[j];
    for (k = 0; k < 5; k++) {
      s += x[j + (k + 1) * 48];
    }

    y[j] = s;
  }
}

//
// Arguments    : const emxArray_real_T *x
//                double y[45]
// Return Type  : void
//
void c_sum(const emxArray_real_T *x, double y[45])
{
  int vlen;
  int i;
  int xoffset;
  double s;
  int k;
  if (x->size[0] == 0) {
    memset(&y[0], 0, 45U * sizeof(double));
  } else {
    vlen = x->size[0];
    for (i = 0; i < 45; i++) {
      xoffset = i * vlen;
      s = x->data[xoffset];
      for (k = 2; k <= vlen; k++) {
        s += x->data[(xoffset + k) - 1];
      }

      y[i] = s;
    }
  }
}

//
// Arguments    : const double x_data[]
//                const int x_size[1]
// Return Type  : double
//
double d_sum(const double x_data[], const int x_size[1])
{
  double y;
  int k;
  if (x_size[0] == 0) {
    y = 0.0;
  } else {
    y = x_data[0];
    for (k = 2; k <= x_size[0]; k++) {
      y += x_data[k - 1];
    }
  }

  return y;
}

//
// Arguments    : const emxArray_real_T *x
//                double y[1035]
// Return Type  : void
//
void e_sum(const emxArray_real_T *x, double y[1035])
{
  int vlen;
  int i;
  int xoffset;
  double s;
  int k;
  if (x->size[0] == 0) {
    memset(&y[0], 0, 1035U * sizeof(double));
  } else {
    vlen = x->size[0];
    for (i = 0; i < 1035; i++) {
      xoffset = i * vlen;
      s = x->data[xoffset];
      for (k = 2; k <= vlen; k++) {
        s += x->data[(xoffset + k) - 1];
      }

      y[i] = s;
    }
  }
}

//
// Arguments    : const double x[768]
//                double y[128]
// Return Type  : void
//
void sum(const double x[768], double y[128])
{
  int j;
  double s;
  int k;
  for (j = 0; j < 128; j++) {
    s = x[j];
    for (k = 0; k < 5; k++) {
      s += x[j + ((k + 1) << 7)];
    }

    y[j] = s;
  }
}

//
// File trailer for sum.cpp
//
// [EOF]
//
