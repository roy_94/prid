//
// File: abs.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "abs.h"

// Function Definitions

//
// Arguments    : const double x[768]
//                double y[768]
// Return Type  : void
//
void b_abs(const double x[768], double y[768])
{
  int k;
  for (k = 0; k < 768; k++) {
    y[k] = fabs(x[k]);
  }
}

//
// Arguments    : const double x[288]
//                double y[288]
// Return Type  : void
//
void c_abs(const double x[288], double y[288])
{
  int k;
  for (k = 0; k < 288; k++) {
    y[k] = fabs(x[k]);
  }
}

//
// File trailer for abs.cpp
//
// [EOF]
//
