//
// File: xdotc.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XDOTC_H
#define XDOTC_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern double b_xdotc(int n, const double x[2116], int ix0, const double y[2116],
                      int iy0);
extern double xdotc(int n, const double x[81], int ix0, const double y[81], int
                    iy0);

#endif

//
// File trailer for xdotc.h
//
// [EOF]
//
