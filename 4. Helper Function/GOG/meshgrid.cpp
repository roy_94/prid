//
// File: meshgrid.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "meshgrid.h"

// Function Definitions

//
// Arguments    : const double x_data[]
//                const int x_size[2]
//                const double y_data[]
//                const int y_size[2]
//                double xx_data[]
//                int xx_size[2]
//                double yy_data[]
//                int yy_size[2]
// Return Type  : void
//
void meshgrid(const double x_data[], const int x_size[2], const double y_data[],
              const int y_size[2], double xx_data[], int xx_size[2], double
              yy_data[], int yy_size[2])
{
  int itilerow;
  double a_data[1];
  if ((x_size[1] == 0) || (y_size[1] == 0)) {
    xx_size[0] = 0;
    xx_size[1] = 0;
    yy_size[0] = 0;
    yy_size[1] = 0;
  } else {
    for (itilerow = 0; itilerow < 1; itilerow++) {
      a_data[0] = x_data[0];
    }

    xx_size[0] = (signed char)y_size[1];
    xx_size[1] = 1;
    for (itilerow = 1; itilerow <= y_size[1]; itilerow++) {
      xx_data[itilerow - 1] = a_data[0];
    }

    yy_size[0] = (signed char)y_size[1];
    yy_size[1] = 1;
    for (itilerow = 0; itilerow + 1 <= y_size[1]; itilerow++) {
      yy_data[itilerow] = y_data[itilerow];
    }
  }
}

//
// File trailer for meshgrid.cpp
//
// [EOF]
//
