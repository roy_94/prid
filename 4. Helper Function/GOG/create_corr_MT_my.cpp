//
// File: create_corr_MT_my.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "create_corr_MT_my.h"
#include "GOG_C1_emxutil.h"

// Function Definitions

//
// Arguments    : const emxArray_real_T *F
//                emxArray_real_T *b
// Return Type  : void
//
void b_create_corr_MT_my(const emxArray_real_T *F, emxArray_real_T *b)
{
  int i13;
  int loop_ub;
  int k;
  int i;
  int j;
  int b_loop_ub;
  int b_i;
  int i14;
  double tmp_data[1536];
  i13 = b->size[0] * b->size[1] * b->size[2];
  b->size[0] = F->size[0];
  b->size[1] = F->size[1];
  b->size[2] = 1035;
  emxEnsureCapacity((emxArray__common *)b, i13, (int)sizeof(double));
  loop_ub = F->size[0] * F->size[1] * 1035;
  for (i13 = 0; i13 < loop_ub; i13++) {
    b->data[i13] = 0.0;
  }

  k = 0;
  for (i = 0; i < 45; i++) {
    for (j = 0; j <= 44 - i; j++) {
      loop_ub = F->size[0];
      b_loop_ub = F->size[1];
      b_i = i + j;
      for (i13 = 0; i13 < b_loop_ub; i13++) {
        for (i14 = 0; i14 < loop_ub; i14++) {
          tmp_data[i14 + loop_ub * i13] = F->data[(i14 + F->size[0] * i13) +
            F->size[0] * F->size[1] * b_i];
        }
      }

      for (i13 = 0; i13 < b_loop_ub; i13++) {
        for (i14 = 0; i14 < loop_ub; i14++) {
          b->data[(i14 + b->size[0] * i13) + b->size[0] * b->size[1] * k] =
            tmp_data[i14 + loop_ub * i13] * F->data[(i14 + F->size[0] * i13) +
            F->size[0] * F->size[1] * i];
        }
      }

      k++;
    }
  }
}

//
// Arguments    : const double F[49152]
//                double b[221184]
// Return Type  : void
//
void create_corr_MT_my(const double F[49152], double b[221184])
{
  int k;
  int i;
  int j;
  int b_i;
  int i8;
  int i9;
  memset(&b[0], 0, 221184U * sizeof(double));
  k = 0;
  for (i = 0; i < 8; i++) {
    for (j = 0; j <= 7 - i; j++) {
      b_i = i + j;
      for (i8 = 0; i8 < 48; i8++) {
        for (i9 = 0; i9 < 128; i9++) {
          b[(i9 + (i8 << 7)) + 6144 * k] = F[(i9 + (i8 << 7)) + 6144 * b_i] * F
            [(i9 + (i8 << 7)) + 6144 * i];
        }
      }

      k++;
    }
  }
}

//
// File trailer for create_corr_MT_my.cpp
//
// [EOF]
//
