//
// File: logm_my.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "logm_my.h"
#include "svd.h"
#include "bsxfun.h"
#include "log.h"
#include "diag.h"
#include "isdiag.h"

// Function Definitions

//
// LOGM  Matrix logarithm.
//    L = LOGM(A) is the principal matrix logarithm A, the inverse of expm(A).
//
//    L is the unique logarithm with which every eigenvalue has imaginary
//    part lying strictly between -pi and pi. If A is singular or has any
//    real eigenvalues on the negative axis then the principal logarithm is
//    undefined, a non-principal logarithm is computed, and a warning message
//    is displayed.
//
//    [L,EXITFLAG] = LOGM(A) returns a scalar EXITFLAG that describes
//    the exit condition of LOGM:
//    EXITFLAG = 0: successful completion of algorithm.
//    EXITFLAG = 1: too many matrix square roots needed.
//                  Computed L may still be accurate, however.
//
//    Class support for input A:
//       float: double, single
//
//    See also EXPM, SQRTM, FUNM.
// Arguments    : const double A[2116]
//                double L[2116]
// Return Type  : void
//
void b_logm_my(const double A[2116], double L[2116])
{
  double Q[2116];
  double dv11[46];
  double b_A[2116];
  int i15;
  double b_L[2116];
  int i16;
  int i17;

  //    References:
  //    A. H. Al-Mohy and Nicholas J. Higham, Improved inverse scaling and
  //       squaring algorithms for the matrix logarithm, SIAM J. Sci. Comput.,
  //       34(4), (2012), pp. C153-C169.
  //    A. H. Al-Mohy, Nicholas J. Higham and Samuel D. Relton, Computing the
  //       Frechet derivative of the matrix logarithm and estimating the
  //       condition number, SIAM J. Sci. Comput., 35(4), (2013), C394-C410.
  //
  //    Nicholas J. Higham and Samuel D. Relton
  //    Copyright 1984-2015 The MathWorks, Inc.
  //  validateattributes(A, {'double', 'single'}, {'finite', 'square'});
  //  maxroots = 100;
  //  Check for triangularity.
  //  [Q, T] = schur(A);
  b_svd(A, Q, L);

  //  stayReal = isreal(A);
  //  Compute the logarithm.
  if (isdiag(L)) {
    //  Check if T is diagonal.
    //      if any(real(d) <= 0 & imag(d) == 0)
    //          warning(message('MATLAB:logm:nonPosRealEig'))   % not supported
    //      end
    diag(L, dv11);
    b_log(dv11);
    c_bsxfun(Q, dv11, b_A);
    for (i15 = 0; i15 < 46; i15++) {
      for (i16 = 0; i16 < 46; i16++) {
        L[i15 + 46 * i16] = 0.0;
        for (i17 = 0; i17 < 46; i17++) {
          L[i15 + 46 * i16] += b_A[i15 + 46 * i17] * Q[i16 + 46 * i17];
        }
      }
    }

    for (i15 = 0; i15 < 46; i15++) {
      for (i16 = 0; i16 < 46; i16++) {
        b_L[i16 + 46 * i15] = (L[i16 + 46 * i15] + L[i15 + 46 * i16]) / 2.0;
      }
    }

    for (i15 = 0; i15 < 46; i15++) {
      memcpy(&L[i15 * 46], &b_L[i15 * 46], 46U * sizeof(double));
    }
  } else {
    //      n = size(T,1);
    //      % Check for negative real eigenvalues.
    //      ei = eig(T);        % ordeig not supported
    //      warns = any(ei == 0);
    //  %     if any(real(ei) < 0 & imag(ei) == 0 )
    //  %         warns = true;
    //  %         if stayReal
    //  %
    //  %         disp('jgdjkdgh');
    //  %         [Q, T] = rsf2csf(Q, T);
    //  %         end
    //  %     end
    //  %     if warns
    //  %         warning(message('MATLAB:logm:nonPosRealEig'));    % not supported 
    //  %         s = struct('identifier', {'MATLAB:illConditionedMatrix',...
    //  %                    'MATLAB:nearlySingularMatrix'}, 'state', 'off');
    //  %         warn = warning(s);                           % not supported
    //  %         w = onCleanup(@()warning(warn));             % onCleanup not supported 
    //  %     end
    //
    //      % Get block structure of Schur factor.
    //
    //      blockformat = qtri_struct(T);
    //
    //  Get parameters.
    //      [s, m, Troot, exitflag] = logm_params(T, maxroots);
    //
    //      % Compute Troot - I = T(1/2^s) - I more accurately.
    //      Troot = recompute_diag_blocks_sqrt(Troot, T, blockformat, s);
    //
    //      % Compute Pade approximant.
    //      L = pade_approx(Troot, m);
    //
    //      % Scale back up.
    //      L = 2^s * L;
    //
    //      % Recompute diagonal blocks.
    //      L = recompute_diag_blocks_log(L, T, blockformat);
    //
    //      if ~schurInput
    //          L = Q*L*Q';
    //      end
  }

  //  Subfunctions
  //  function [s, m, Troot, exitflag] = logm_params(T, maxroots)
  //  exitflag = 0;
  //  n = size(T,1);
  //  I = eye(n,class(T));
  //
  //  xvals = [1.586970738772063e-005
  //           2.313807884242979e-003
  //           1.938179313533253e-002
  //           6.209171588994762e-002
  //           1.276404810806775e-001
  //           2.060962623452836e-001
  //           2.879093714241194e-001];
  //
  //  mmax = 7;
  //  foundm = false;
  //
  //  % Get initial s0 so that T^(1/2^s0) < xvals(mmax).
  //  s = 0;
  //  d = eig(T);                       % not supported
  //  while norm(d-1, 'inf') > xvals(mmax) && s < maxroots
  //      d = sqrt(d);
  //      s = s + 1;
  //  end
  //  s0 = s;
  //  % if s == maxroots
  //  %     warning(message('MATLAB:logm:TooManyMatrixSquareRoots'))    % not supported 
  //  %     exitflag = 1;
  //  % end
  //
  //  Troot = T;
  //  for k = 1:min(s, maxroots)
  //      Troot = sqrtm_tri(Troot);
  //  end
  //
  //  % Compute value of s and m needed.
  //  TrootmI = Troot - I;
  //  d2 = normAm(TrootmI, 2)^(1/2);
  //  d3 = normAm(TrootmI, 3)^(1/3);
  //  a2 = max(d2, d3);
  //  if a2 <= xvals(2)
  //      m = find(a2 <= xvals(1:2), 1);
  //      foundm = true;
  //  end
  //  p = 0;
  //  while ~foundm
  //      more = false; % More norm checks needed.
  //      if s > s0
  //          d3 = normAm(TrootmI, 3)^(1/3);
  //      end
  //      d4 = normAm(TrootmI, 4)^(1/4);
  //      a3 = max(d3, d4);
  //      if a3 <= xvals(mmax)
  //          j = find(a3 <= xvals(3:mmax), 1) + 2;
  //          if j <= 6
  //              m = j;
  //              break
  //          else
  //              if a3/2 <= xvals(5) && p < 2
  //                  more = true;
  //                  p = p + 1;
  //              end
  //          end
  //      end
  //      if ~more
  //          d5 = normAm(TrootmI, 5)^(1/5);
  //          a4 = max(d4, d5);
  //          eta = min(a3, a4);
  //          if eta <= xvals(mmax)
  //              m = find(eta <= xvals(6:mmax), 1) + 5;
  //              break
  //          end
  //      end
  //      if s == maxroots
  //  %         if exitflag == 0
  //  %             warning(message('MATLAB:logm:TooManyMatrixSquareRoots'))    % not supported 
  //  %         end
  //          exitflag = 1;
  //          m = mmax; % No good value found so take largest.
  //          break;
  //      end
  //      Troot = sqrtm_tri(Troot);
  //      TrootmI = Troot - I;
  //      s = s + 1;
  //  end
  //  end
  //
  //  function u = unwinding(z)
  //  %unwinding Unwinding number of z.
  //     u = ceil( (imag(z) - pi)/(2*pi) );
  //  end
  //  function L = recompute_diag_blocks_log(L, T, blockStruct)
  //  % Recomputes diagonal blocks of L = log(T) accurately.
  //  n = length(T);
  //  last_block = 0;
  //  for j = 1:n-1
  //      switch blockStruct(j)
  //          case 0 % Not start of a block.
  //              if last_block ~= 0
  //                  last_block = 0;
  //                  continue;
  //              else
  //                  last_block = 0;
  //                  L(j,j) = log(T(j,j));
  //              end
  //          case 1 % Start of upper-tri block.
  //              last_block = 1;
  //              a1 = T(j,j);
  //              a2 = T(j+1,j+1);
  //              loga1 = log(a1);
  //              loga2 = log(a2);
  //              L(j,j) = loga1;
  //              L(j+1,j+1) = loga2;
  //              if (a1 < 0 && imag(a1)==0) || (a2 < 0 && imag(a1)==0)
  //                  % Problems with 2 x 2 formula for (1,2) block
  //                  % since atanh is nonstandard, just redo diagonal part.
  //                  continue;
  //              end
  //              if a1 == a2
  //                  a12 = T(j,j+1)/a1;
  //              elseif abs(a1) < 0.5*abs(a2) || abs(a2) < 0.5*abs(a1)
  //                  a12 =  T(j,j+1) * (loga2 - loga1) / (a2 - a1);
  //              else % Close eigenvalues.
  //                  z = (a2-a1)/(a2+a1);
  //                  dd = (2*atanh(z) + 2*pi*1i*(unwinding(loga2-loga1))) / (a2-a1); 
  //                  a12 = T(j,j+1)*dd;
  //              end
  //              L(j,j+1) = a12;
  //          case 2 % Start of quasi-tri block.
  //              last_block = 2;
  //              f = 0.5 * log(T(j,j)^2 - T(j,j+1)*T(j+1,j));
  //              t = atan2(sqrt(-T(j,j+1)*T(j+1,j)), T(j,j))/sqrt(-T(j,j+1)*T(j+1,j)); 
  //              L(j,j) = f;
  //              L(j+1,j) = t*T(j+1,j);
  //              L(j,j+1) = t*T(j,j+1);
  //              L(j+1,j+1) = f;
  //      end
  //  end
  //  end
  //  function val = sqrt_obo(a, s)
  //  % sqrt_obo Computes a^(1/2^s) - 1 accurately.
  //  if s == 0
  //      val = a-1;
  //      return
  //  end
  //  % If s ~= 0 perform computation avoiding subtractive cancellation.
  //  n0 = s;
  //  if angle(a) >= pi/2
  //      a = sqrt(a); n0 = s-1;
  //  end
  //  z0 = a - 1;
  //  a = sqrt(a);
  //  r = 1 + a;
  //  for i=1:n0-1
  //      a = sqrt(a);
  //      r = r*(1+a);
  //  end
  //  val = z0/r;
  //  end
  //  function Troot = recompute_diag_blocks_sqrt(Troot, T, blockStruct, s)
  //  % Recomputes diagonal blocks of T = X^(1/2^s) - 1 more accurately.
  //  n = length(T);
  //  last_block = 0;
  //  for j = 1:n-1
  //      switch blockStruct(j)
  //          case 0 % Not start of a block.
  //              if last_block ~= 0
  //                  last_block = 0;
  //                  continue
  //              else % In a 1x1 block.
  //                  last_block = 0;
  //                  a = T(j,j);
  //                  Troot(j,j) = sqrt_obo(a, s);
  //              end
  //          otherwise
  //              % In a 2x2 block.
  //              last_block = blockStruct(j);
  //              I = eye(2, class(T));
  //              if s == 0
  //                  Troot(j:j+1,j:j+1) = T(j:j+1,j:j+1) - I;
  //                  continue
  //              end
  //              A = sqrtm_tbt(T(j:j+1,j:j+1));
  //
  //              Z0 = A - I;
  //              if s == 1
  //                  Troot(j:j+1,j:j+1) = Z0;
  //                  continue
  //              end
  //              A = sqrtm_tbt(A);
  //              P = A + I;
  //              for i = 1:s - 2
  //                  A = sqrtm_tbt(A);
  //                  P = P*(I + A);
  //              end
  //              Troot(j:j+1,j:j+1) = Z0 / P;
  //              % If block is upper triangular recompute the (1,2) element.
  //              % Skip when T(j,j) or T(j+1,j+1) < 0 since the implementation
  //              % of atanh is nonstandard.
  //              if T(j+1,j) == 0 && T(j,j) >= 0 && T(j+1,j+1) >= 0
  //                  Troot(j,j+1) = powerm2by2(T(j:j+1,j:j+1), 1/(2^s));
  //              end
  //      end
  //  end
  //  % If last diagonal entry is not in a block it will have been missed.
  //  if blockStruct(end) == 0
  //      a = T(n,n);
  //      Troot(n,n) = sqrt_obo(a, s);
  //  end
  //  end
  //  function x12 = powerm2by2(A,p)
  //  %powerm2by2 Power of 2-by-2 upper triangular matrix.
  //  %   powerm2by2(A,p) is the (1,2) element of the pth power of the 2 x 2
  //  %   upper triangular matrix A, where p is an arbitrary real number.
  //
  //  a1 = A(1,1);
  //  a2 = A(2,2);
  //
  //  if a1 == a2
  //     x12 = p*A(1,2)*a1^(p-1);
  //  elseif abs(a1) < 0.5*abs(a2) || abs(a2) < 0.5*abs(a1)
  //     a1p = a1^p;
  //     a2p = a2^p;
  //     x12 =  A(1,2) * (a2p - a1p) / (a2 - a1);
  //  else % Close eigenvalues.
  //     loga1 = log(a1);
  //     loga2 = log(a2);
  //     w = atanh((a2-a1)/(a2+a1)) + 1i*pi*unwinding(loga2-loga1);
  //     dd = 2 * exp(p*(loga1+loga2)/2) * sinh(p*w) / (a2-a1);
  //     x12 = A(1,2) * dd;
  //  end
  //  end
  //  function [x, w] = gauss_legendre(n)
  //  %gauss_legendre Nodes and weights for Gauss-Legendre quadrature.
  //  %   [x,w] = gauss_legendre(n) computes the nodes x and weights w
  //  %   for n-point Gauss-Legendre quadrature.
  //
  //  % Reference:
  //  % G. H. Golub and J. H. Welsch, Calculation of Gauss quadrature
  //  %    rules, Math. Comp., 23(106):221-230, 1969.
  //  k = 1:n-1;
  //  v = k./sqrt((2*k).^2-1);
  //  [V, x] = eig(diag(v,-1)+diag(v,1),'vector');
  //  w = 2*(V(1,:)'.^2);
  //  end
  //  function L = pade_approx(T, m)
  //  %pade_approx Pade approximation to log(1 + T) via partial fractions.
  //      [nodes, wts] = gauss_legendre(m);
  //      % Convert from [-1,1] to [0,1].
  //      nodes = (nodes + 1)/2;
  //      wts = wts/2;
  //      n = size(T,1);
  //      L = zeros(n,class(T));
  //      for j=1:m
  //          K = nodes(j).*T;
  //          K(1:n+1:end) = K(1:n+1:end) + 1;
  //          L = L + wts(j) .* (K \ T);
  //      end
  //  end
}

//
// LOGM  Matrix logarithm.
//    L = LOGM(A) is the principal matrix logarithm A, the inverse of expm(A).
//
//    L is the unique logarithm with which every eigenvalue has imaginary
//    part lying strictly between -pi and pi. If A is singular or has any
//    real eigenvalues on the negative axis then the principal logarithm is
//    undefined, a non-principal logarithm is computed, and a warning message
//    is displayed.
//
//    [L,EXITFLAG] = LOGM(A) returns a scalar EXITFLAG that describes
//    the exit condition of LOGM:
//    EXITFLAG = 0: successful completion of algorithm.
//    EXITFLAG = 1: too many matrix square roots needed.
//                  Computed L may still be accurate, however.
//
//    Class support for input A:
//       float: double, single
//
//    See also EXPM, SQRTM, FUNM.
// Arguments    : const double A[81]
//                double L[81]
// Return Type  : void
//
void logm_my(const double A[81], double L[81])
{
  double Q[81];
  boolean_T p;
  int ak;
  boolean_T exitg2;
  int bk;
  int exitg1;
  double d[9];
  double c[81];
  int ck;
  int k;
  double b_L[81];

  //    References:
  //    A. H. Al-Mohy and Nicholas J. Higham, Improved inverse scaling and
  //       squaring algorithms for the matrix logarithm, SIAM J. Sci. Comput.,
  //       34(4), (2012), pp. C153-C169.
  //    A. H. Al-Mohy, Nicholas J. Higham and Samuel D. Relton, Computing the
  //       Frechet derivative of the matrix logarithm and estimating the
  //       condition number, SIAM J. Sci. Comput., 35(4), (2013), C394-C410.
  //
  //    Nicholas J. Higham and Samuel D. Relton
  //    Copyright 1984-2015 The MathWorks, Inc.
  //  validateattributes(A, {'double', 'single'}, {'finite', 'square'});
  //  maxroots = 100;
  //  Check for triangularity.
  //  [Q, T] = schur(A);
  svd(A, Q, L);

  //  stayReal = isreal(A);
  //  Compute the logarithm.
  p = true;
  ak = 1;
  exitg2 = false;
  while ((!exitg2) && (ak < 10)) {
    bk = 1;
    do {
      exitg1 = 0;
      if (bk < 10) {
        if ((bk != ak) && (!(L[(bk + 9 * (ak - 1)) - 1] == 0.0))) {
          p = false;
          exitg1 = 1;
        } else {
          bk++;
        }
      } else {
        ak++;
        exitg1 = 2;
      }
    } while (exitg1 == 0);

    if (exitg1 == 1) {
      exitg2 = true;
    }
  }

  if (p) {
    //  Check if T is diagonal.
    //      if any(real(d) <= 0 & imag(d) == 0)
    //          warning(message('MATLAB:logm:nonPosRealEig'))   % not supported
    //      end
    for (ak = 0; ak < 9; ak++) {
      d[ak] = log(L[ak * 10]);
    }

    ak = 0;
    bk = 0;
    for (ck = 0; ck <= 73; ck += 9) {
      for (k = 0; k < 9; k++) {
        c[ck + k] = Q[ak + k] * d[bk];
      }

      ak += 9;
      bk++;
    }

    for (ak = 0; ak < 9; ak++) {
      for (bk = 0; bk < 9; bk++) {
        L[ak + 9 * bk] = 0.0;
        for (ck = 0; ck < 9; ck++) {
          L[ak + 9 * bk] += c[ak + 9 * ck] * Q[bk + 9 * ck];
        }
      }
    }

    for (ak = 0; ak < 9; ak++) {
      for (bk = 0; bk < 9; bk++) {
        b_L[bk + 9 * ak] = (L[bk + 9 * ak] + L[ak + 9 * bk]) / 2.0;
      }
    }

    for (ak = 0; ak < 9; ak++) {
      memcpy(&L[ak * 9], &b_L[ak * 9], 9U * sizeof(double));
    }
  } else {
    //      n = size(T,1);
    //      % Check for negative real eigenvalues.
    //      ei = eig(T);        % ordeig not supported
    //      warns = any(ei == 0);
    //  %     if any(real(ei) < 0 & imag(ei) == 0 )
    //  %         warns = true;
    //  %         if stayReal
    //  %
    //  %         disp('jgdjkdgh');
    //  %         [Q, T] = rsf2csf(Q, T);
    //  %         end
    //  %     end
    //  %     if warns
    //  %         warning(message('MATLAB:logm:nonPosRealEig'));    % not supported 
    //  %         s = struct('identifier', {'MATLAB:illConditionedMatrix',...
    //  %                    'MATLAB:nearlySingularMatrix'}, 'state', 'off');
    //  %         warn = warning(s);                           % not supported
    //  %         w = onCleanup(@()warning(warn));             % onCleanup not supported 
    //  %     end
    //
    //      % Get block structure of Schur factor.
    //
    //      blockformat = qtri_struct(T);
    //
    //  Get parameters.
    //      [s, m, Troot, exitflag] = logm_params(T, maxroots);
    //
    //      % Compute Troot - I = T(1/2^s) - I more accurately.
    //      Troot = recompute_diag_blocks_sqrt(Troot, T, blockformat, s);
    //
    //      % Compute Pade approximant.
    //      L = pade_approx(Troot, m);
    //
    //      % Scale back up.
    //      L = 2^s * L;
    //
    //      % Recompute diagonal blocks.
    //      L = recompute_diag_blocks_log(L, T, blockformat);
    //
    //      if ~schurInput
    //          L = Q*L*Q';
    //      end
  }

  //  Subfunctions
  //  function [s, m, Troot, exitflag] = logm_params(T, maxroots)
  //  exitflag = 0;
  //  n = size(T,1);
  //  I = eye(n,class(T));
  //
  //  xvals = [1.586970738772063e-005
  //           2.313807884242979e-003
  //           1.938179313533253e-002
  //           6.209171588994762e-002
  //           1.276404810806775e-001
  //           2.060962623452836e-001
  //           2.879093714241194e-001];
  //
  //  mmax = 7;
  //  foundm = false;
  //
  //  % Get initial s0 so that T^(1/2^s0) < xvals(mmax).
  //  s = 0;
  //  d = eig(T);                       % not supported
  //  while norm(d-1, 'inf') > xvals(mmax) && s < maxroots
  //      d = sqrt(d);
  //      s = s + 1;
  //  end
  //  s0 = s;
  //  % if s == maxroots
  //  %     warning(message('MATLAB:logm:TooManyMatrixSquareRoots'))    % not supported 
  //  %     exitflag = 1;
  //  % end
  //
  //  Troot = T;
  //  for k = 1:min(s, maxroots)
  //      Troot = sqrtm_tri(Troot);
  //  end
  //
  //  % Compute value of s and m needed.
  //  TrootmI = Troot - I;
  //  d2 = normAm(TrootmI, 2)^(1/2);
  //  d3 = normAm(TrootmI, 3)^(1/3);
  //  a2 = max(d2, d3);
  //  if a2 <= xvals(2)
  //      m = find(a2 <= xvals(1:2), 1);
  //      foundm = true;
  //  end
  //  p = 0;
  //  while ~foundm
  //      more = false; % More norm checks needed.
  //      if s > s0
  //          d3 = normAm(TrootmI, 3)^(1/3);
  //      end
  //      d4 = normAm(TrootmI, 4)^(1/4);
  //      a3 = max(d3, d4);
  //      if a3 <= xvals(mmax)
  //          j = find(a3 <= xvals(3:mmax), 1) + 2;
  //          if j <= 6
  //              m = j;
  //              break
  //          else
  //              if a3/2 <= xvals(5) && p < 2
  //                  more = true;
  //                  p = p + 1;
  //              end
  //          end
  //      end
  //      if ~more
  //          d5 = normAm(TrootmI, 5)^(1/5);
  //          a4 = max(d4, d5);
  //          eta = min(a3, a4);
  //          if eta <= xvals(mmax)
  //              m = find(eta <= xvals(6:mmax), 1) + 5;
  //              break
  //          end
  //      end
  //      if s == maxroots
  //  %         if exitflag == 0
  //  %             warning(message('MATLAB:logm:TooManyMatrixSquareRoots'))    % not supported 
  //  %         end
  //          exitflag = 1;
  //          m = mmax; % No good value found so take largest.
  //          break;
  //      end
  //      Troot = sqrtm_tri(Troot);
  //      TrootmI = Troot - I;
  //      s = s + 1;
  //  end
  //  end
  //
  //  function u = unwinding(z)
  //  %unwinding Unwinding number of z.
  //     u = ceil( (imag(z) - pi)/(2*pi) );
  //  end
  //  function L = recompute_diag_blocks_log(L, T, blockStruct)
  //  % Recomputes diagonal blocks of L = log(T) accurately.
  //  n = length(T);
  //  last_block = 0;
  //  for j = 1:n-1
  //      switch blockStruct(j)
  //          case 0 % Not start of a block.
  //              if last_block ~= 0
  //                  last_block = 0;
  //                  continue;
  //              else
  //                  last_block = 0;
  //                  L(j,j) = log(T(j,j));
  //              end
  //          case 1 % Start of upper-tri block.
  //              last_block = 1;
  //              a1 = T(j,j);
  //              a2 = T(j+1,j+1);
  //              loga1 = log(a1);
  //              loga2 = log(a2);
  //              L(j,j) = loga1;
  //              L(j+1,j+1) = loga2;
  //              if (a1 < 0 && imag(a1)==0) || (a2 < 0 && imag(a1)==0)
  //                  % Problems with 2 x 2 formula for (1,2) block
  //                  % since atanh is nonstandard, just redo diagonal part.
  //                  continue;
  //              end
  //              if a1 == a2
  //                  a12 = T(j,j+1)/a1;
  //              elseif abs(a1) < 0.5*abs(a2) || abs(a2) < 0.5*abs(a1)
  //                  a12 =  T(j,j+1) * (loga2 - loga1) / (a2 - a1);
  //              else % Close eigenvalues.
  //                  z = (a2-a1)/(a2+a1);
  //                  dd = (2*atanh(z) + 2*pi*1i*(unwinding(loga2-loga1))) / (a2-a1); 
  //                  a12 = T(j,j+1)*dd;
  //              end
  //              L(j,j+1) = a12;
  //          case 2 % Start of quasi-tri block.
  //              last_block = 2;
  //              f = 0.5 * log(T(j,j)^2 - T(j,j+1)*T(j+1,j));
  //              t = atan2(sqrt(-T(j,j+1)*T(j+1,j)), T(j,j))/sqrt(-T(j,j+1)*T(j+1,j)); 
  //              L(j,j) = f;
  //              L(j+1,j) = t*T(j+1,j);
  //              L(j,j+1) = t*T(j,j+1);
  //              L(j+1,j+1) = f;
  //      end
  //  end
  //  end
  //  function val = sqrt_obo(a, s)
  //  % sqrt_obo Computes a^(1/2^s) - 1 accurately.
  //  if s == 0
  //      val = a-1;
  //      return
  //  end
  //  % If s ~= 0 perform computation avoiding subtractive cancellation.
  //  n0 = s;
  //  if angle(a) >= pi/2
  //      a = sqrt(a); n0 = s-1;
  //  end
  //  z0 = a - 1;
  //  a = sqrt(a);
  //  r = 1 + a;
  //  for i=1:n0-1
  //      a = sqrt(a);
  //      r = r*(1+a);
  //  end
  //  val = z0/r;
  //  end
  //  function Troot = recompute_diag_blocks_sqrt(Troot, T, blockStruct, s)
  //  % Recomputes diagonal blocks of T = X^(1/2^s) - 1 more accurately.
  //  n = length(T);
  //  last_block = 0;
  //  for j = 1:n-1
  //      switch blockStruct(j)
  //          case 0 % Not start of a block.
  //              if last_block ~= 0
  //                  last_block = 0;
  //                  continue
  //              else % In a 1x1 block.
  //                  last_block = 0;
  //                  a = T(j,j);
  //                  Troot(j,j) = sqrt_obo(a, s);
  //              end
  //          otherwise
  //              % In a 2x2 block.
  //              last_block = blockStruct(j);
  //              I = eye(2, class(T));
  //              if s == 0
  //                  Troot(j:j+1,j:j+1) = T(j:j+1,j:j+1) - I;
  //                  continue
  //              end
  //              A = sqrtm_tbt(T(j:j+1,j:j+1));
  //
  //              Z0 = A - I;
  //              if s == 1
  //                  Troot(j:j+1,j:j+1) = Z0;
  //                  continue
  //              end
  //              A = sqrtm_tbt(A);
  //              P = A + I;
  //              for i = 1:s - 2
  //                  A = sqrtm_tbt(A);
  //                  P = P*(I + A);
  //              end
  //              Troot(j:j+1,j:j+1) = Z0 / P;
  //              % If block is upper triangular recompute the (1,2) element.
  //              % Skip when T(j,j) or T(j+1,j+1) < 0 since the implementation
  //              % of atanh is nonstandard.
  //              if T(j+1,j) == 0 && T(j,j) >= 0 && T(j+1,j+1) >= 0
  //                  Troot(j,j+1) = powerm2by2(T(j:j+1,j:j+1), 1/(2^s));
  //              end
  //      end
  //  end
  //  % If last diagonal entry is not in a block it will have been missed.
  //  if blockStruct(end) == 0
  //      a = T(n,n);
  //      Troot(n,n) = sqrt_obo(a, s);
  //  end
  //  end
  //  function x12 = powerm2by2(A,p)
  //  %powerm2by2 Power of 2-by-2 upper triangular matrix.
  //  %   powerm2by2(A,p) is the (1,2) element of the pth power of the 2 x 2
  //  %   upper triangular matrix A, where p is an arbitrary real number.
  //
  //  a1 = A(1,1);
  //  a2 = A(2,2);
  //
  //  if a1 == a2
  //     x12 = p*A(1,2)*a1^(p-1);
  //  elseif abs(a1) < 0.5*abs(a2) || abs(a2) < 0.5*abs(a1)
  //     a1p = a1^p;
  //     a2p = a2^p;
  //     x12 =  A(1,2) * (a2p - a1p) / (a2 - a1);
  //  else % Close eigenvalues.
  //     loga1 = log(a1);
  //     loga2 = log(a2);
  //     w = atanh((a2-a1)/(a2+a1)) + 1i*pi*unwinding(loga2-loga1);
  //     dd = 2 * exp(p*(loga1+loga2)/2) * sinh(p*w) / (a2-a1);
  //     x12 = A(1,2) * dd;
  //  end
  //  end
  //  function [x, w] = gauss_legendre(n)
  //  %gauss_legendre Nodes and weights for Gauss-Legendre quadrature.
  //  %   [x,w] = gauss_legendre(n) computes the nodes x and weights w
  //  %   for n-point Gauss-Legendre quadrature.
  //
  //  % Reference:
  //  % G. H. Golub and J. H. Welsch, Calculation of Gauss quadrature
  //  %    rules, Math. Comp., 23(106):221-230, 1969.
  //  k = 1:n-1;
  //  v = k./sqrt((2*k).^2-1);
  //  [V, x] = eig(diag(v,-1)+diag(v,1),'vector');
  //  w = 2*(V(1,:)'.^2);
  //  end
  //  function L = pade_approx(T, m)
  //  %pade_approx Pade approximation to log(1 + T) via partial fractions.
  //      [nodes, wts] = gauss_legendre(m);
  //      % Convert from [-1,1] to [0,1].
  //      nodes = (nodes + 1)/2;
  //      wts = wts/2;
  //      n = size(T,1);
  //      L = zeros(n,class(T));
  //      for j=1:m
  //          K = nodes(j).*T;
  //          K(1:n+1:end) = K(1:n+1:end) + 1;
  //          L = L + wts(j) .* (K \ T);
  //      end
  //  end
}

//
// File trailer for logm_my.cpp
//
// [EOF]
//
