//
// File: rgb2ycbcr.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "rgb2ycbcr.h"
#include "imlincomb.h"

// Function Definitions

//
// Arguments    : const unsigned char X[18432]
//                unsigned char ycbcr[18432]
// Return Type  : void
//
void rgb2ycbcr(const unsigned char X[18432], unsigned char ycbcr[18432])
{
  int p;
  static const double T[9] = { 0.25678823529411759, -0.1482235294117647,
    0.4392156862745098, 0.50412941176470583, -0.2909921568627451,
    -0.36778823529411764, 0.097905882352941176, 0.4392156862745098,
    -0.071427450980392146 };

  static const unsigned char offset[3] = { 16U, 128U, 128U };

  for (p = 0; p < 3; p++) {
    imlincomb(T[p], *(unsigned char (*)[6144])&X[0], T[3 + p], *(unsigned char (*)
               [6144])&X[6144], T[6 + p], *(unsigned char (*)[6144])&X[12288],
              (double)offset[p], *(unsigned char (*)[6144])&ycbcr[6144 * p]);
  }
}

//
// File trailer for rgb2ycbcr.cpp
//
// [EOF]
//
