//
// File: xaxpy.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "xaxpy.h"

// Function Definitions

//
// Arguments    : int n
//                double a
//                const double x[81]
//                int ix0
//                double y[9]
//                int iy0
// Return Type  : void
//
void b_xaxpy(int n, double a, const double x[81], int ix0, double y[9], int iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * x[ix];
      ix++;
      iy++;
    }
  }
}

//
// Arguments    : int n
//                double a
//                const double x[9]
//                int ix0
//                double y[81]
//                int iy0
// Return Type  : void
//
void c_xaxpy(int n, double a, const double x[9], int ix0, double y[81], int iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * x[ix];
      ix++;
      iy++;
    }
  }
}

//
// Arguments    : int n
//                double a
//                int ix0
//                double y[2116]
//                int iy0
// Return Type  : void
//
void d_xaxpy(int n, double a, int ix0, double y[2116], int iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * y[ix];
      ix++;
      iy++;
    }
  }
}

//
// Arguments    : int n
//                double a
//                const double x[2116]
//                int ix0
//                double y[46]
//                int iy0
// Return Type  : void
//
void e_xaxpy(int n, double a, const double x[2116], int ix0, double y[46], int
             iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * x[ix];
      ix++;
      iy++;
    }
  }
}

//
// Arguments    : int n
//                double a
//                const double x[46]
//                int ix0
//                double y[2116]
//                int iy0
// Return Type  : void
//
void f_xaxpy(int n, double a, const double x[46], int ix0, double y[2116], int
             iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * x[ix];
      ix++;
      iy++;
    }
  }
}

//
// Arguments    : int n
//                double a
//                int ix0
//                double y[81]
//                int iy0
// Return Type  : void
//
void xaxpy(int n, double a, int ix0, double y[81], int iy0)
{
  int ix;
  int iy;
  int k;
  if (a == 0.0) {
  } else {
    ix = ix0 - 1;
    iy = iy0 - 1;
    for (k = 0; k < n; k++) {
      y[iy] += a * y[ix];
      ix++;
      iy++;
    }
  }
}

//
// File trailer for xaxpy.cpp
//
// [EOF]
//
