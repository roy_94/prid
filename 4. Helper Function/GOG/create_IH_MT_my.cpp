//
// File: create_IH_MT_my.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "create_IH_MT_my.h"

// Function Definitions

//
// Arguments    : const double F[49152]
//                double IH[50568]
// Return Type  : void
//
void b_create_IH_MT_my(const double F[49152], double IH[50568])
{
  int i;
  int j;
  double b_IH[8];
  int i11;
  memset(&IH[0], 0, 50568U * sizeof(double));
  for (i = 0; i < 129; i++) {
    for (j = 0; j < 49; j++) {
      if ((1 + j == 1) || (1 + i == 1)) {
        for (i11 = 0; i11 < 8; i11++) {
          IH[(i + 129 * j) + 6321 * i11] = 0.0;
        }
      } else {
        for (i11 = 0; i11 < 8; i11++) {
          b_IH[i11] = ((IH[((i + 129 * j) + 6321 * i11) - 1] + IH[(i + 129 * (j
            - 1)) + 6321 * i11]) - IH[((i + 129 * (j - 1)) + 6321 * i11) - 1]) +
            F[((i + ((j - 1) << 7)) + 6144 * i11) - 1];
        }

        for (i11 = 0; i11 < 8; i11++) {
          IH[(i + 129 * j) + 6321 * i11] = b_IH[i11];
        }
      }
    }
  }
}

//
// Arguments    : const double F[221184]
//                double IH[227556]
// Return Type  : void
//
void create_IH_MT_my(const double F[221184], double IH[227556])
{
  int i;
  int j;
  double b_IH[36];
  int i10;
  memset(&IH[0], 0, 227556U * sizeof(double));
  for (i = 0; i < 129; i++) {
    for (j = 0; j < 49; j++) {
      if ((1 + j == 1) || (1 + i == 1)) {
        for (i10 = 0; i10 < 36; i10++) {
          IH[(i + 129 * j) + 6321 * i10] = 0.0;
        }
      } else {
        for (i10 = 0; i10 < 36; i10++) {
          b_IH[i10] = ((IH[((i + 129 * j) + 6321 * i10) - 1] + IH[(i + 129 * (j
            - 1)) + 6321 * i10]) - IH[((i + 129 * (j - 1)) + 6321 * i10) - 1]) +
            F[((i + ((j - 1) << 7)) + 6144 * i10) - 1];
        }

        for (i10 = 0; i10 < 36; i10++) {
          IH[(i + 129 * j) + 6321 * i10] = b_IH[i10];
        }
      }
    }
  }
}

//
// File trailer for create_IH_MT_my.cpp
//
// [EOF]
//
