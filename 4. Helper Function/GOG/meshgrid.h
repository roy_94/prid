//
// File: meshgrid.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef MESHGRID_H
#define MESHGRID_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void meshgrid(const double x_data[], const int x_size[2], const double
                     y_data[], const int y_size[2], double xx_data[], int
                     xx_size[2], double yy_data[], int yy_size[2]);

#endif

//
// File trailer for meshgrid.h
//
// [EOF]
//
