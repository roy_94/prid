//
// File: halfvec_my.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef HALFVEC_MY_H
#define HALFVEC_MY_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_halfvec_my(const double a11[2116], double logPcells[1081]);
extern void halfvec_my(const double a11[81], double logPcells[45]);

#endif

//
// File trailer for halfvec_my.h
//
// [EOF]
//
