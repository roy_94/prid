//
// File: det.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "det.h"
#include "xgetrf.h"

// Function Definitions

//
// Arguments    : const double x[2025]
// Return Type  : double
//
double b_det(const double x[2025])
{
  double y;
  double b_x[2025];
  int ipiv[45];
  boolean_T isodd;
  int k;
  memcpy(&b_x[0], &x[0], 2025U * sizeof(double));
  xgetrf(b_x, ipiv);
  y = b_x[0];
  isodd = false;
  for (k = 0; k < 44; k++) {
    y *= b_x[(k + 45 * (k + 1)) + 1];
    if (ipiv[k] > 1 + k) {
      isodd = !isodd;
    }
  }

  if (isodd) {
    y = -y;
  }

  return y;
}

//
// Arguments    : const double x[64]
// Return Type  : double
//
double det(const double x[64])
{
  double y;
  double A[64];
  signed char ipiv[8];
  int i12;
  int j;
  int c;
  boolean_T isodd;
  int iy;
  int jy;
  int ix;
  double smax;
  double s;
  int b_j;
  int ijA;
  memcpy(&A[0], &x[0], sizeof(double) << 6);
  for (i12 = 0; i12 < 8; i12++) {
    ipiv[i12] = (signed char)(1 + i12);
  }

  for (j = 0; j < 7; j++) {
    c = j * 9;
    iy = 0;
    ix = c;
    smax = fabs(A[c]);
    for (jy = 2; jy <= 8 - j; jy++) {
      ix++;
      s = fabs(A[ix]);
      if (s > smax) {
        iy = jy - 1;
        smax = s;
      }
    }

    if (A[c + iy] != 0.0) {
      if (iy != 0) {
        ipiv[j] = (signed char)((j + iy) + 1);
        ix = j;
        iy += j;
        for (jy = 0; jy < 8; jy++) {
          smax = A[ix];
          A[ix] = A[iy];
          A[iy] = smax;
          ix += 8;
          iy += 8;
        }
      }

      i12 = (c - j) + 8;
      for (iy = c + 1; iy + 1 <= i12; iy++) {
        A[iy] /= A[c];
      }
    }

    iy = c + 9;
    jy = c + 8;
    for (b_j = 1; b_j <= 7 - j; b_j++) {
      smax = A[jy];
      if (A[jy] != 0.0) {
        ix = c + 1;
        i12 = (iy - j) + 7;
        for (ijA = iy; ijA + 1 <= i12; ijA++) {
          A[ijA] += A[ix] * -smax;
          ix++;
        }
      }

      jy += 8;
      iy += 8;
    }
  }

  y = A[0];
  isodd = false;
  for (jy = 0; jy < 7; jy++) {
    y *= A[(jy + ((jy + 1) << 3)) + 1];
    if (ipiv[jy] > 1 + jy) {
      isodd = !isodd;
    }
  }

  if (isodd) {
    y = -y;
  }

  return y;
}

//
// File trailer for det.cpp
//
// [EOF]
//
