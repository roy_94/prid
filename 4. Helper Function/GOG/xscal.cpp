//
// File: xscal.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "xscal.h"

// Function Definitions

//
// Arguments    : double a
//                double x[2116]
//                int ix0
// Return Type  : void
//
void b_xscal(double a, double x[2116], int ix0)
{
  int k;
  for (k = ix0; k <= ix0 + 45; k++) {
    x[k - 1] *= a;
  }
}

//
// Arguments    : double a
//                double x[81]
//                int ix0
// Return Type  : void
//
void xscal(double a, double x[81], int ix0)
{
  int k;
  for (k = ix0; k <= ix0 + 8; k++) {
    x[k - 1] *= a;
  }
}

//
// File trailer for xscal.cpp
//
// [EOF]
//
