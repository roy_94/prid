//
// File: imlincomb.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef IMLINCOMB_H
#define IMLINCOMB_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void imlincomb(double varargin_1, const unsigned char varargin_2[6144],
                      double varargin_3, const unsigned char varargin_4[6144],
                      double varargin_5, const unsigned char varargin_6[6144],
                      double varargin_7, unsigned char Z[6144]);

#endif

//
// File trailer for imlincomb.h
//
// [EOF]
//
