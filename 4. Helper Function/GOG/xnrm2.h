//
// File: xnrm2.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XNRM2_H
#define XNRM2_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern double b_xnrm2(int n, const double x[9], int ix0);
extern double c_xnrm2(int n, const double x[2116], int ix0);
extern double d_xnrm2(int n, const double x[46], int ix0);
extern double xnrm2(int n, const double x[81], int ix0);

#endif

//
// File trailer for xnrm2.h
//
// [EOF]
//
