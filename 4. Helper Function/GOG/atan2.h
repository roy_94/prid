//
// File: atan2.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef ATAN2_H
#define ATAN2_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_atan2(const double y[6144], const double x[6144], double r[6144]);

#endif

//
// File trailer for atan2.h
//
// [EOF]
//
