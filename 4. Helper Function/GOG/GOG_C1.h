//
// File: GOG_C1.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef GOG_C1_H
#define GOG_C1_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void GOG_C1(unsigned char X[18432], double feature_vec_data[7567]);

#endif

//
// File trailer for GOG_C1.h
//
// [EOF]
//
