//
// File: logm_my.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef LOGM_MY_H
#define LOGM_MY_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_logm_my(const double A[2116], double L[2116]);
extern void logm_my(const double A[81], double L[81]);

#endif

//
// File trailer for logm_my.h
//
// [EOF]
//
