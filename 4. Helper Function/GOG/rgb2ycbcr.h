//
// File: rgb2ycbcr.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef RGB2YCBCR_H
#define RGB2YCBCR_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void rgb2ycbcr(const unsigned char X[18432], unsigned char ycbcr[18432]);

#endif

//
// File trailer for rgb2ycbcr.h
//
// [EOF]
//
