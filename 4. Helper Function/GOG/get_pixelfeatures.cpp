//
// File: get_pixelfeatures.cpp

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "get_pixelfeatures.h"
#include "repmat.h"
#include "get_gradmap.h"
#include "rgb2ycbcr.h"

// Function Definitions

//
// get_pixelfeaturres.m
//    extract pixelfeatures map defined by lfparam
//
//    Input:
//           <X>: input RGb image. Size: [h, w, 3]
//           [lfparam]: parameters.
//
//    Output:
//           <F>: pixel feature map. Size: [h, w, lfparam.num_element]
// Arguments    : const unsigned char X[18432]
//                double F[49152]
// Return Type  : void
//
void get_pixelfeatures(const unsigned char X[18432], double F[49152])
{
  static double PY[6144];
  int tmpy;
  int i3;
  unsigned char uv1[18432];
  static double img_ycbcr[18432];
  static double qori[24576];
  static double mag[6144];
  int i4;
  static double dv6[24576];
  int i5;

  //  y
  memset(&PY[0], 0, 6144U * sizeof(double));
  for (tmpy = 0; tmpy < 128; tmpy++) {
    for (i3 = 0; i3 < 48; i3++) {
      PY[tmpy + (i3 << 7)] = 1.0 + (double)tmpy;
    }
  }

  for (i3 = 0; i3 < 6144; i3++) {
    PY[i3] /= 128.0;
  }

  for (i3 = 0; i3 < 48; i3++) {
    memcpy(&F[i3 << 7], &PY[i3 << 7], sizeof(double) << 7);
  }

  //  M_theta
  rgb2ycbcr(X, uv1);
  for (i3 = 0; i3 < 18432; i3++) {
    img_ycbcr[i3] = uv1[i3];
  }

  for (i3 = 0; i3 < 48; i3++) {
    for (i4 = 0; i4 < 128; i4++) {
      img_ycbcr[i4 + (i3 << 7)] = (img_ycbcr[i4 + (i3 << 7)] - 16.0) / 235.0;
    }
  }

  get_gradmap(*(double (*)[6144])&img_ycbcr[0], qori, PY, mag);
  repmat(mag, dv6);
  for (i3 = 0; i3 < 4; i3++) {
    for (i4 = 0; i4 < 48; i4++) {
      for (i5 = 0; i5 < 128; i5++) {
        F[(i5 + (i4 << 7)) + 6144 * (1 + i3)] = qori[(i5 + (i4 << 7)) + 6144 *
          i3] * dv6[(i5 + (i4 << 7)) + 6144 * i3];
      }
    }
  }

  //  RGb
  for (i3 = 0; i3 < 3; i3++) {
    for (i4 = 0; i4 < 48; i4++) {
      for (i5 = 0; i5 < 128; i5++) {
        F[(i5 + (i4 << 7)) + 6144 * (5 + i3)] = (double)X[(i5 + (i4 << 7)) +
          6144 * i3] / 255.0;
      }
    }
  }
}

//
// File trailer for get_pixelfeatures.cpp
//
// [EOF]
//
