//
// File: xscal.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XSCAL_H
#define XSCAL_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_xscal(double a, double x[2116], int ix0);
extern void xscal(double a, double x[81], int ix0);

#endif

//
// File trailer for xscal.h
//
// [EOF]
//
