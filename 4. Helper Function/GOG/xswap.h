//
// File: xswap.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XSWAP_H
#define XSWAP_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_xswap(double x[2025], int ix0, int iy0);
extern void c_xswap(double x[2116], int ix0, int iy0);
extern void xswap(double x[81], int ix0, int iy0);

#endif

//
// File trailer for xswap.h
//
// [EOF]
//
