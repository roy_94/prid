//
// File: halfvec_my.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "halfvec_my.h"

// Function Definitions

//
// Arguments    : const double a11[2116]
//                double logPcells[1081]
// Return Type  : void
//
void b_halfvec_my(const double a11[2116], double logPcells[1081])
{
  int k;
  int i;
  int j;
  int b_j;
  memset(&logPcells[0], 0, 1081U * sizeof(double));
  k = 0;
  for (i = 0; i < 46; i++) {
    for (j = 0; j <= 45 - i; j++) {
      b_j = i + j;
      logPcells[k] = a11[i + 46 * b_j];
      if (1 + i != b_j + 1) {
        logPcells[k] *= 1.4142135623730951;
      }

      k++;
    }
  }
}

//
// Arguments    : const double a11[81]
//                double logPcells[45]
// Return Type  : void
//
void halfvec_my(const double a11[81], double logPcells[45])
{
  int k;
  int i;
  int j;
  int b_j;
  memset(&logPcells[0], 0, 45U * sizeof(double));
  k = 0;
  for (i = 0; i < 9; i++) {
    for (j = 0; j <= 8 - i; j++) {
      b_j = i + j;
      logPcells[k] = a11[i + 9 * b_j];
      if (1 + i != b_j + 1) {
        logPcells[k] *= 1.4142135623730951;
      }

      k++;
    }
  }
}

//
// File trailer for halfvec_my.cpp
//
// [EOF]
//
