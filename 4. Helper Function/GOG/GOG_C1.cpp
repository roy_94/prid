

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "halfvec_my.h"
#include "logm_my.h"
#include "det.h"
#include "eye.h"
#include "trace.h"
#include "vec2mat_my1.h"
#include "sum.h"
#include "GOG_C1_emxutil.h"
#include "repmat.h"
#include "create_corr_MT_my.h"
#include "meshgrid.h"
#include "create_IH_MT_my.h"
#include "get_pixelfeatures.h"
//#include "imresize.h"
#include "GOG_C1_rtwutil.h"
#include "GOG_PARA.h"

// Function Definitions


void GOG_C1(unsigned char X[18432], double feature_vec_data[7567])
{
  int feature_vec_size[1];
  static unsigned char b_X[18432];
  static unsigned char uv0[18432];
  static double F[49152];
  static double dv0[221184];
  static double IH_Corr[227556];
  static double IH_F[50568];
  signed char xlefts[1536];
  short xrights[1536];
  signed char yups[1536];
  unsigned char ydowns[1536];
  int k;
  static double IH_Corr2[227556];
  static double IH_F2[50568];
  
  short points1[1536];
  short points2[1536];
  short points3[1536];
  short points4[1536];
  int i;
  static double sumFCorrs[55296];
  int i0;
  int kd;
  static double sumFs[12288];
  static double logPcells[69120];
  double meanVec[45];
  static double F2[69120];
  double b_sumFCorrs[36];
  double S_mat[64];
  double S[64];
  double weightmap[1536];
  int x;
  double y_data[7];
  double y;
  double b_y_data[7];
  double b_y;
  int y_size[2];
  int b_y_size[2];
  double b_meanVec[8];
  double cols_data[7];
  int cols_size[2];
  double rows_data[7];
  int rows_size[2];
  double xrights_data[7];
  double b_S[64];
  int cols_idx_0;
  double ydowns_data[7];
  int logPcells1_size_idx_0;
  double c_S[81];
  static double logPcells1_data[7567];
  double c_y[81];
  emxArray_real_T *F2local;
  emxArray_real_T *tmp1;
  emxArray_real_T *FCorr2;
  double a11[81];
  emxArray_real_T *b_FCorr2;
  emxArray_real_T *r0;
  emxArray_real_T *r1;
  emxArray_real_T *r2;
  emxArray_real_T *r3;
  int i1;
  int i2;
  int b_F2local;
  int nx;
  double weightlocal_data[1536];
  int weightlocal_size[1];
  double dv1[45];
  double b_x[1035];
  double c_x[1035];
  double d_S[2025];
  double dv2[2025];
  double e_S[2025];
  static double f_S[2116];
  static double d_y[2116];
  static double b_a11[2116];
  double dv3[1081];
  memcpy(&b_X[0], &X[0], 18432U * sizeof(unsigned char));

  //  Select Feature\n  [Y HOG RGB LAB HSV nRnG]
  //  reguralization paramter of covariance
  //  patch extraction interval
  //  patch size (k x k pixels)
  //  patch weight  0 -- not use,  1 -- use.
  //  number of horizontal strips
  // Y(1) HOG(4) RGB(3) LAB(3) HSV(3) nRnG(2)
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //  7x1 horizontal strips
  // % 1. Pixel Feature Extraction
 // imresize(b_X, uv0);
  get_pixelfeatures(b_X, F);
  create_corr_MT_my(F, dv0);
  create_IH_MT_my(dv0, IH_Corr);
  b_create_IH_MT_my(F, IH_F);
  for (k = 0; k < 1536; k++) {
    if (varargin_1[k] >= 2) {
      xlefts[k] = varargin_1[k];
    } else {
      xlefts[k] = 2;
    }

    if (b_varargin_1[k] <= 49) {
      xrights[k] = b_varargin_1[k];
    } else {
      xrights[k] = 49;
    }

    if (c_varargin_1[k] >= 2) {
      yups[k] = c_varargin_1[k];
    } else {
      yups[k] = 2;
    }

    if (d_varargin_1[k] <= 129) {
      ydowns[k] = d_varargin_1[k];
    } else {
      ydowns[k] = 129;
    }
  }

  memcpy(&IH_Corr2[0], &IH_Corr[0], 227556U * sizeof(double));
  memcpy(&IH_F2[0], &IH_F[0], 50568U * sizeof(double));
  for (i = 0; i < 1536; i++) {
    points1[i] = (short)((xrights[i] - 1) * 129 + ydowns[i]);
    points2[i] = (short)(((xlefts[i] - 2) * 129 + yups[i]) - 1);
    points3[i] = (short)(((xrights[i] - 1) * 129 + yups[i]) - 1);
    points4[i] = (short)((xlefts[i] - 2) * 129 + ydowns[i]);
  }

  for (i0 = 0; i0 < 36; i0++) {
    for (kd = 0; kd < 1536; kd++) {
      sumFCorrs[kd + 1536 * i0] = ((IH_Corr2[(points1[kd] + 6321 * i0) - 1] +
        IH_Corr2[(points2[kd] + 6321 * i0) - 1]) - IH_Corr2[(points3[kd] + 6321 *
        i0) - 1]) - IH_Corr2[(points4[kd] + 6321 * i0) - 1];
    }
  }

  //  get values from IH
  for (i0 = 0; i0 < 8; i0++) {
    for (kd = 0; kd < 1536; kd++) {
      sumFs[kd + 1536 * i0] = ((IH_F2[(points1[kd] + 6321 * i0) - 1] + IH_F2
        [(points2[kd] + 6321 * i0) - 1]) - IH_F2[(points3[kd] + 6321 * i0) - 1])
        - IH_F2[(points4[kd] + 6321 * i0) - 1];
    }
  }

  //  get values from IH
  for (i0 = 0; i0 < 1536; i0++) {
    xrights[i0] = (short)((short)((short)(xrights[i0] - xlefts[i0]) + 1) *
                          ((ydowns[i0] - yups[i0]) + 1));
  }

  for (i = 0; i < 1536; i++) {
    for (i0 = 0; i0 < 36; i0++) {
      b_sumFCorrs[i0] = sumFCorrs[i + 1536 * i0];
    }

    vec2mat_my1(b_sumFCorrs, S_mat);
    for (i0 = 0; i0 < 8; i0++) {
      for (kd = 0; kd < 8; kd++) {
        S[i0 + (kd << 3)] = (S_mat[i0 + (kd << 3)] - sumFs[i + 1536 * i0] *
                             sumFs[i + 1536 * kd] / (double)xrights[i]) /
          ((double)xrights[i] - 1.0);
      }
    }

    //  covariance matrix by integral image
    y = trace(S);
    eye(S_mat);
    if (y >= 0.01) {
      b_y = y;
    } else {
      b_y = 0.01;
    }

    y = 0.001 * b_y;
    for (i0 = 0; i0 < 64; i0++) {
      S[i0] += y * S_mat[i0];
    }

    //  regularizaiton
    for (i0 = 0; i0 < 8; i0++) {
      b_meanVec[i0] = sumFs[i + 1536 * i0] / (double)xrights[i];
    }

    //  mean vector
    y = rt_powd_snf(det(S), -0.1111111111111111);

    //  patch Gaussian matrix
    for (i0 = 0; i0 < 8; i0++) {
      for (kd = 0; kd < 8; kd++) {
        b_S[i0 + (kd << 3)] = S[i0 + (kd << 3)] + b_meanVec[i0] * b_meanVec[kd];
      }
    }

    for (i0 = 0; i0 < 8; i0++) {
      memcpy(&c_S[i0 * 9], &b_S[i0 << 3], sizeof(double) << 3);
      c_S[72 + i0] = b_meanVec[i0];
      c_S[8 + 9 * i0] = b_meanVec[i0];
    }

    c_S[80] = 1.0;
    for (i0 = 0; i0 < 9; i0++) {
      for (kd = 0; kd < 9; kd++) {
        c_y[kd + 9 * i0] = y * c_S[kd + 9 * i0];
      }
    }

    logm_my(c_y, a11);
    halfvec_my(a11, meanVec);
    for (i0 = 0; i0 < 45; i0++) {
      logPcells[i + 1536 * i0] = meanVec[i0];
    }
  }

  memcpy(&F2[0], &logPcells[0], 69120U * sizeof(double));

  //
  // % 3.  Region Gaussians
  //  setup of patch weight
  for (x = 0; x < 24; x++) {
    y = exp(-((double)((x - 11) * (x - 11)) / 72.0)) / 15.039769647786002;
    for (i0 = 0; i0 < 64; i0++) {
      weightmap[i0 + (x << 6)] = y;
    }
  }

  y_data[0] = 1.0;
  b_y_data[0] = 1.0;
  b_y_data[6] = 49.0;
  for (k = 0; k < 2; k++) {
    y_size[k] = 1;
    b_y_size[k] = 1 + 6 * k;
    kd = (k + 1) << 3;
    b_y_data[k + 1] = 1.0 + (double)kd;
    b_y_data[5 - k] = 49.0 - (double)kd;
  }

  b_y_data[3] = 25.0;
  meshgrid(y_data, y_size, b_y_data, b_y_size, cols_data, cols_size, rows_data,
           rows_size);
  kd = cols_size[0] * cols_size[1];
  for (i0 = 0; i0 < kd; i0++) {
    xrights_data[i0] = (cols_data[i0] + 24.0) - 1.0;
  }

  kd = rows_size[0] * rows_size[1];
  for (i0 = 0; i0 < kd; i0++) {
    ydowns_data[i0] = (rows_data[i0] + 16.0) - 1.0;
  }

  cols_idx_0 = cols_size[0] * cols_size[1];
  logPcells1_size_idx_0 = cols_size[0] * cols_size[1];
  kd = cols_size[0] * cols_size[1] * 1081;
  for (i0 = 0; i0 < kd; i0++) {
    logPcells1_data[i0] = 0.0;
  }

  i = 0;
  emxInit_real_T(&F2local, 2);
  emxInit_real_T1(&tmp1, 3);
  emxInit_real_T1(&FCorr2, 3);
  emxInit_real_T(&b_FCorr2, 2);
  emxInit_real_T(&r0, 2);
  emxInit_real_T(&r1, 2);
  emxInit_real_T(&r2, 2);
  emxInit_real_T(&r3, 2);
  while (i <= cols_size[0] * cols_size[1] - 1) {
    if (rows_data[i] > ydowns_data[i]) {
      i0 = 1;
      kd = 1;
    } else {
      i0 = (int)rows_data[i];
      kd = (int)ydowns_data[i] + 1;
    }

    if (rows_data[i] > ydowns_data[i]) {
      i1 = 1;
      i2 = 1;
    } else {
      i1 = (int)rows_data[i];
      i2 = (int)ydowns_data[i] + 1;
    }

    b_F2local = (int)xrights_data[i];
    nx = (kd - i0) * (int)xrights_data[i];
    weightlocal_size[0] = (short)((kd - i0) * (int)xrights_data[i]);
    for (k = 0; k + 1 <= nx; k++) {
      weightlocal_data[k] = weightmap[((i0 + k % (kd - i0)) + (k / (kd - i0) <<
        6)) - 1];
    }

    nx = (i2 - i1) * (int)xrights_data[i] * 45;
    i0 = F2local->size[0] * F2local->size[1];
    F2local->size[0] = (short)((i2 - i1) * (int)xrights_data[i]);
    F2local->size[1] = 45;
    emxEnsureCapacity((emxArray__common *)F2local, i0, (int)sizeof(double));
    for (k = 0; k + 1 <= nx; k++) {
      i0 = i2 - i1;
      F2local->data[k] = F2[(((i1 + k % (i2 - i1)) + (k / i0 % b_F2local << 6))
        + 1536 * (k / (i0 * b_F2local))) - 1];
    }

    b_repmat(weightlocal_data, weightlocal_size, r0);
    i0 = r3->size[0] * r3->size[1];
    r3->size[0] = r0->size[0];
    r3->size[1] = 45;
    emxEnsureCapacity((emxArray__common *)r3, i0, (int)sizeof(double));
    kd = r0->size[0] * r0->size[1];
    for (i0 = 0; i0 < kd; i0++) {
      r3->data[i0] = r0->data[i0] * F2local->data[i0];
    }

    c_sum(r3, dv1);
    memcpy(&meanVec[0], &dv1[0], 45U * sizeof(double));
    y = d_sum(weightlocal_data, weightlocal_size);
    for (i0 = 0; i0 < 45; i0++) {
      meanVec[i0] /= y;
    }

    //  mean vector
    c_repmat(meanVec, (double)F2local->size[0], r0);
    i0 = F2local->size[0] * F2local->size[1];
    F2local->size[1] = 45;
    emxEnsureCapacity((emxArray__common *)F2local, i0, (int)sizeof(double));
    kd = F2local->size[0];
    b_F2local = F2local->size[1];
    kd *= b_F2local;
    for (i0 = 0; i0 < kd; i0++) {
      F2local->data[i0] -= r0->data[i0];
    }

    nx = F2local->size[0] * 45;
    i0 = tmp1->size[0] * tmp1->size[1] * tmp1->size[2];
    tmp1->size[0] = (signed char)(i2 - i1);
    tmp1->size[1] = (signed char)(int)xrights_data[i];
    tmp1->size[2] = 45;
    emxEnsureCapacity((emxArray__common *)tmp1, i0, (int)sizeof(double));
    for (k = 0; k + 1 <= nx; k++) {
      tmp1->data[k] = F2local->data[k];
    }

    //      FCorr2 = create_corr_MT(tmp1);
    b_create_corr_MT_my(tmp1, FCorr2);
    kd = FCorr2->size[0] * FCorr2->size[1];
    nx = FCorr2->size[0] * FCorr2->size[1] * 1035;
    i0 = b_FCorr2->size[0] * b_FCorr2->size[1];
    b_FCorr2->size[0] = (short)kd;
    b_FCorr2->size[1] = 1035;
    emxEnsureCapacity((emxArray__common *)b_FCorr2, i0, (int)sizeof(double));
    for (k = 0; k + 1 <= nx; k++) {
      b_FCorr2->data[k] = FCorr2->data[k];
    }

    d_repmat(weightlocal_data, weightlocal_size, r1);
    i0 = r2->size[0] * r2->size[1];
    r2->size[0] = r1->size[0];
    r2->size[1] = 1035;
    emxEnsureCapacity((emxArray__common *)r2, i0, (int)sizeof(double));
    kd = r1->size[0] * r1->size[1];
    for (i0 = 0; i0 < kd; i0++) {
      r2->data[i0] = r1->data[i0] * b_FCorr2->data[i0];
    }

    e_sum(r2, b_x);
    y = d_sum(weightlocal_data, weightlocal_size);
    for (i0 = 0; i0 < 1035; i0++) {
      c_x[i0] = b_x[i0] / y;
    }

    b_vec2mat_my1(c_x, d_S);

    //  covariance matrix
    b_eye(dv2);
    y = 0.001 * b_trace(d_S);
    for (i0 = 0; i0 < 2025; i0++) {
      d_S[i0] += y * dv2[i0];
    }

    //  regularizaiton
    y = b_det(d_S);
    y = rt_powd_snf(y, -0.021739130434782608);

    //  apply region Gaussian matrix
    for (i0 = 0; i0 < 45; i0++) {
      for (kd = 0; kd < 45; kd++) {
        e_S[i0 + 45 * kd] = d_S[i0 + 45 * kd] + meanVec[i0] * meanVec[kd];
      }
    }

    for (i0 = 0; i0 < 45; i0++) {
      memcpy(&f_S[i0 * 46], &e_S[i0 * 45], 45U * sizeof(double));
      f_S[2070 + i0] = meanVec[i0];
      f_S[45 + 46 * i0] = meanVec[i0];
    }

    f_S[2115] = 1.0;
    for (i0 = 0; i0 < 46; i0++) {
      for (kd = 0; kd < 46; kd++) {
        d_y[kd + 46 * i0] = y * f_S[kd + 46 * i0];
      }
    }

    b_logm_my(d_y, b_a11);
    b_halfvec_my(b_a11, dv3);
    for (i0 = 0; i0 < 1081; i0++) {
      logPcells1_data[i + logPcells1_size_idx_0 * i0] = dv3[i0];
    }

    i++;
  }

  emxFree_real_T(&r3);
  emxFree_real_T(&r2);
  emxFree_real_T(&r1);
  emxFree_real_T(&r0);
  emxFree_real_T(&b_FCorr2);
  emxFree_real_T(&FCorr2);
  emxFree_real_T(&tmp1);
  emxFree_real_T(&F2local);

  //  logPcells = cell2mat_my( cellfun( @(x) halfvec(logm_my(x))', Pcells, 'un', 0) ); % apply log Euclidean and half-vectorization 
  nx = cols_idx_0 * 1081;
  feature_vec_size[0] = (short)(cols_idx_0 * 1081);
  for (k = 0; k + 1 <= nx; k++) {
    feature_vec_data[k] = logPcells1_data[k];
  }

  //  concatenate feature vector of each grid
  //   feature_vec = GOG_C11(logPcells1); % extract GOG
}

//
// File trailer for GOG_C1.cpp
//
// [EOF]
//
