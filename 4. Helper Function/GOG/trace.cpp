//
// File: trace.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "trace.h"

// Function Definitions

//
// Arguments    : const double a[2025]
// Return Type  : double
//
double b_trace(const double a[2025])
{
  double t;
  int k;
  t = 0.0;
  for (k = 0; k < 45; k++) {
    t += a[k + 45 * k];
  }

  return t;
}

//
// Arguments    : const double a[64]
// Return Type  : double
//
double trace(const double a[64])
{
  double t;
  int k;
  t = 0.0;
  for (k = 0; k < 8; k++) {
    t += a[k + (k << 3)];
  }

  return t;
}

//
// File trailer for trace.cpp
//
// [EOF]
//
