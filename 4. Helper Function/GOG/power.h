//
// File: power.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef POWER_H
#define POWER_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_power(const double a[768], double y[768]);
extern void c_power(const double a[288], double y[288]);
extern void d_power(const double a[288], double y[288]);
extern void e_power(const double a[6144], double y[6144]);
extern void power(const double a[768], double y[768]);

#endif

//
// File trailer for power.h
//
// [EOF]
//
