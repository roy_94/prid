//
// File: imfilter.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef IMFILTER_H
#define IMFILTER_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_imfilter(const double varargin_1[6144], double b[6144]);
extern void imfilter(const double varargin_1[6144], double b[6144]);

#endif

//
// File trailer for imfilter.h
//
// [EOF]
//
