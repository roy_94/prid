//
// File: vec2mat_my1.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef VEC2MAT_MY1_H
#define VEC2MAT_MY1_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_vec2mat_my1(const double vec[1035], double mat[2025]);
extern void vec2mat_my1(const double vec[36], double mat[64]);

#endif

//
// File trailer for vec2mat_my1.h
//
// [EOF]
//
