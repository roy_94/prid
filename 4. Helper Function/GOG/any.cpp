//
// File: any.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "any.h"

// Function Definitions

//
// Arguments    : const double x[768]
//                boolean_T y[6]
// Return Type  : void
//
void any(const double x[768], boolean_T y[6])
{
  int i2;
  int iy;
  int i;
  int i1;
  boolean_T exitg1;
  boolean_T b0;
  for (i2 = 0; i2 < 6; i2++) {
    y[i2] = false;
  }

  i2 = 0;
  iy = -1;
  for (i = 0; i < 6; i++) {
    i1 = i2;
    i2 += 128;
    iy++;
    exitg1 = false;
    while ((!exitg1) && (i1 + 1 <= i2)) {
      if ((x[i1] == 0.0) || rtIsNaN(x[i1])) {
        b0 = true;
      } else {
        b0 = false;
      }

      if (!b0) {
        y[iy] = true;
        exitg1 = true;
      } else {
        i1++;
      }
    }
  }
}

//
// File trailer for any.cpp
//
// [EOF]
//
