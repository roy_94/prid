//
// File: get_pixelfeatures.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef GET_PIXELFEATURES_H
#define GET_PIXELFEATURES_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void get_pixelfeatures(const unsigned char X[18432], double F[49152]);

#endif

//
// File trailer for get_pixelfeatures.h
//
// [EOF]
//
