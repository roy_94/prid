//
// File: get_gradmap.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef GET_GRADMAP_H
#define GET_GRADMAP_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void get_gradmap(const double X[6144], double qori[24576], double ori
  [6144], double mag[6144]);

#endif

//
// File trailer for get_gradmap.h
//
// [EOF]
//
