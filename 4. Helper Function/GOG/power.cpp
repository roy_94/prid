//
// File: power.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "power.h"
#include "GOG_C1_rtwutil.h"

// Function Definitions

//
// Arguments    : const double a[768]
//                double y[768]
// Return Type  : void
//
void b_power(const double a[768], double y[768])
{
  int k;
  for (k = 0; k < 768; k++) {
    y[k] = rt_powd_snf(a[k], 3.0);
  }
}

//
// Arguments    : const double a[288]
//                double y[288]
// Return Type  : void
//
void c_power(const double a[288], double y[288])
{
  int k;
  for (k = 0; k < 288; k++) {
    y[k] = a[k] * a[k];
  }
}

//
// Arguments    : const double a[288]
//                double y[288]
// Return Type  : void
//
void d_power(const double a[288], double y[288])
{
  int k;
  for (k = 0; k < 288; k++) {
    y[k] = rt_powd_snf(a[k], 3.0);
  }
}

//
// Arguments    : const double a[6144]
//                double y[6144]
// Return Type  : void
//
void e_power(const double a[6144], double y[6144])
{
  double x[6144];
  int k;
  int b_k;
  memcpy(&x[0], &a[0], 6144U * sizeof(double));

#pragma omp parallel for \
 num_threads(omp_get_max_threads()) \
 private(b_k)

  for (k = 1; k < 6145; k++) {
    b_k = k;
    y[b_k - 1] = x[b_k - 1] * x[b_k - 1];
  }
}

//
// Arguments    : const double a[768]
//                double y[768]
// Return Type  : void
//
void power(const double a[768], double y[768])
{
  int k;
  for (k = 0; k < 768; k++) {
    y[k] = a[k] * a[k];
  }
}

//
// File trailer for power.cpp
//
// [EOF]
//
