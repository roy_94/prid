//
// File: isdiag.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "isdiag.h"

// Function Definitions

//
// Arguments    : const double x[2116]
// Return Type  : boolean_T
//
boolean_T isdiag(const double x[2116])
{
  boolean_T p;
  int j;
  boolean_T exitg2;
  int i;
  int exitg1;
  p = true;
  j = 1;
  exitg2 = false;
  while ((!exitg2) && (j < 47)) {
    i = 1;
    do {
      exitg1 = 0;
      if (i < 47) {
        if ((i != j) && (!(x[(i + 46 * (j - 1)) - 1] == 0.0))) {
          p = false;
          exitg1 = 1;
        } else {
          i++;
        }
      } else {
        j++;
        exitg1 = 2;
      }
    } while (exitg1 == 0);

    if (exitg1 == 1) {
      exitg2 = true;
    }
  }

  return p;
}

//
// File trailer for isdiag.cpp
//
// [EOF]
//
