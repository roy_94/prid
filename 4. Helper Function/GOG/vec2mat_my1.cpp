//
// File: vec2mat_my1.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "vec2mat_my1.h"

// Function Definitions

//
// Arguments    : const double vec[1035]
//                double mat[2025]
// Return Type  : void
//
void b_vec2mat_my1(const double vec[1035], double mat[2025])
{
  double k;
  int j;
  int i;
  double b_i;
  memset(&mat[0], 0, 2025U * sizeof(double));
  k = 1.0;
  j = 0;
  while (j + 1 != 1036) {
    for (i = 0; i < (int)(45.0 + (1.0 - k)); i++) {
      b_i = k + (double)i;
      mat[((int)b_i + 45 * ((int)k - 1)) - 1] = vec[j];
      mat[((int)k + 45 * ((int)b_i - 1)) - 1] = vec[j];
      j++;
    }

    k++;
  }
}

//
// Arguments    : const double vec[36]
//                double mat[64]
// Return Type  : void
//
void vec2mat_my1(const double vec[36], double mat[64])
{
  double k;
  int j;
  int i;
  double b_i;
  memset(&mat[0], 0, sizeof(double) << 6);
  k = 1.0;
  j = 0;
  while (j + 1 != 37) {
    for (i = 0; i < (int)(8.0 + (1.0 - k)); i++) {
      b_i = k + (double)i;
      mat[((int)b_i + (((int)k - 1) << 3)) - 1] = vec[j];
      mat[((int)k + (((int)b_i - 1) << 3)) - 1] = vec[j];
      j++;
    }

    k++;
  }
}

//
// File trailer for vec2mat_my1.cpp
//
// [EOF]
//
