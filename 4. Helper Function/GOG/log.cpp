//
// File: log.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "log.h"

// Function Definitions

//
// Arguments    : double x[46]
// Return Type  : void
//
void b_log(double x[46])
{
  int k;
  for (k = 0; k < 46; k++) {
    x[k] = log(x[k]);
  }
}

//
// File trailer for log.cpp
//
// [EOF]
//
