//
// File: xaxpy.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef XAXPY_H
#define XAXPY_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_xaxpy(int n, double a, const double x[81], int ix0, double y[9],
                    int iy0);
extern void c_xaxpy(int n, double a, const double x[9], int ix0, double y[81],
                    int iy0);
extern void d_xaxpy(int n, double a, int ix0, double y[2116], int iy0);
extern void e_xaxpy(int n, double a, const double x[2116], int ix0, double y[46],
                    int iy0);
extern void f_xaxpy(int n, double a, const double x[46], int ix0, double y[2116],
                    int iy0);
extern void xaxpy(int n, double a, int ix0, double y[81], int iy0);

#endif

//
// File trailer for xaxpy.h
//
// [EOF]
//
