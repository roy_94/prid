//
// File: sum.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef SUM_H
#define SUM_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_sum(const double x[288], double y[48]);
extern void c_sum(const emxArray_real_T *x, double y[45]);
extern double d_sum(const double x_data[], const int x_size[1]);
extern void e_sum(const emxArray_real_T *x, double y[1035]);
extern void sum(const double x[768], double y[128]);

#endif

//
// File trailer for sum.h
//
// [EOF]
//
