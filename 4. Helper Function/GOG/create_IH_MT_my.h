//
// File: create_IH_MT_my.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef CREATE_IH_MT_MY_H
#define CREATE_IH_MT_MY_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_create_IH_MT_my(const double F[49152], double IH[50568]);
extern void create_IH_MT_my(const double F[221184], double IH[227556]);

#endif

//
// File trailer for create_IH_MT_my.h
//
// [EOF]
//
