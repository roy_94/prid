//
// File: get_gradmap.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "get_gradmap.h"
#include "floor.h"
#include "sqrt.h"
#include "power.h"
#include "atan2.h"
#include "imfilter.h"

// Function Definitions

//
// X -- input image. Size [h w 1]
//  binnum -- number of graient orientation bins
//  qori -- quantized gradient orientation map (soft voting). Size [h w binnum]
//  ori -- graident orientation map. Size [h w 1]
//  mag -- gradient magnitude map. Size [h w 1]
// Arguments    : const double X[6144]
//                double qori[24576]
//                double ori[6144]
//                double mag[6144]
// Return Type  : void
//
void get_gradmap(const double X[6144], double qori[24576], double ori[6144],
                 double mag[6144])
{
  static double grad_x[6144];
  static double grad_y[6144];
  int i;
  static double IND[6144];
  static double dif1[6144];
  static double weight1[6144];
  double b_dif1;
  double b_grad_x;
  int y;
  double b_IND;
  int x;

  //  gradient filter
  imfilter(X, grad_x);
  b_imfilter(X, grad_y);
  b_atan2(grad_x, grad_y, ori);

  //  gradient orientations
  e_power(grad_x, mag);
  e_power(grad_y, grad_x);
  for (i = 0; i < 6144; i++) {
    ori[i] = (ori[i] + 3.1415926535897931) * 180.0 / 3.1415926535897931;
    mag[i] += grad_x[i];
  }

  b_sqrt(mag);

  //  gradient magnitude
  for (i = 0; i < 6144; i++) {
    IND[i] = ori[i] / 90.0;
  }

  b_floor(IND);
  for (i = 0; i < 6144; i++) {
    b_dif1 = ori[i] - IND[i] * 90.0;
    b_grad_x = (IND[i] + 1.0) * 90.0 - ori[i];
    weight1[i] = b_grad_x / (b_dif1 + b_grad_x);
    b_dif1 /= b_dif1 + b_grad_x;
    dif1[i] = b_dif1;
  }

  memset(&qori[0], 0, 24576U * sizeof(double));
  for (i = 0; i < 6144; i++) {
    b_IND = IND[i];
    if (IND[i] == 4.0) {
      b_IND = 0.0;
    }

    grad_x[i] = b_IND + 1.0;
    grad_y[i] = b_IND + 2.0;
    if (b_IND + 2.0 == 5.0) {
      grad_y[i] = 1.0;
    }
  }

  for (y = 0; y < 128; y++) {
    for (x = 0; x < 48; x++) {
      qori[(y + (x << 7)) + 6144 * ((int)grad_x[y + (x << 7)] - 1)] = weight1[y
        + (x << 7)];
      qori[(y + (x << 7)) + 6144 * ((int)grad_y[y + (x << 7)] - 1)] = dif1[y +
        (x << 7)];
    }
  }
}

//
// File trailer for get_gradmap.cpp
//
// [EOF]
//
