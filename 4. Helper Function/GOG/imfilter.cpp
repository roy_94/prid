//
// File: imfilter.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "imfilter.h"
#include "convn.h"

// Function Declarations
static void b_padImage(const double a_tmp[6144], double a[6240]);
static void padImage(const double a_tmp[6144], double a[6400]);

// Function Definitions

//
// Arguments    : const double a_tmp[6144]
//                double a[6240]
// Return Type  : void
//
static void b_padImage(const double a_tmp[6144], double a[6240])
{
  int j;
  for (j = 0; j < 48; j++) {
    a[130 * j] = 0.0;
    a[129 + 130 * j] = 0.0;
    memcpy(&a[j * 130 + 1], &a_tmp[j << 7], sizeof(double) << 7);
  }
}

//
// Arguments    : const double a_tmp[6144]
//                double a[6400]
// Return Type  : void
//
static void padImage(const double a_tmp[6144], double a[6400])
{
  int i;
  for (i = 0; i < 128; i++) {
    a[i] = 0.0;
    a[6272 + i] = 0.0;
  }

  for (i = 0; i < 48; i++) {
    memcpy(&a[(i << 7) + 128], &a_tmp[i << 7], sizeof(double) << 7);
  }
}

//
// Arguments    : const double varargin_1[6144]
//                double b[6144]
// Return Type  : void
//
void b_imfilter(const double varargin_1[6144], double b[6144])
{
  double dv9[3];
  int i7;
  static double dv10[6240];
  static double result[6240];
  for (i7 = 0; i7 < 3; i7++) {
    dv9[i7] = -1.0 + (double)i7;
  }

  b_padImage(varargin_1, dv10);
  b_convn(dv10, dv9, result);
  for (i7 = 0; i7 < 48; i7++) {
    memcpy(&b[i7 << 7], &result[i7 * 130 + 1], sizeof(double) << 7);
  }
}

//
// Arguments    : const double varargin_1[6144]
//                double b[6144]
// Return Type  : void
//
void imfilter(const double varargin_1[6144], double b[6144])
{
  double dv7[3];
  int i6;
  static double dv8[6400];
  static double result[6400];
  for (i6 = 0; i6 < 3; i6++) {
    dv7[i6] = 1.0 - (double)i6;
  }

  padImage(varargin_1, dv8);
  convn(dv8, dv7, result);
  for (i6 = 0; i6 < 48; i6++) {
    memcpy(&b[i6 << 7], &result[(i6 << 7) + 128], sizeof(double) << 7);
  }
}

//
// File trailer for imfilter.cpp
//
// [EOF]
//
