//
// File: repmat.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef REPMAT_H
#define REPMAT_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_repmat(const double a_data[], const int a_size[1], emxArray_real_T
                     *b);
extern void c_repmat(const double a[45], double varargin_1, emxArray_real_T *b);
extern void d_repmat(const double a_data[], const int a_size[1], emxArray_real_T
                     *b);
extern void repmat(const double a[6144], double b[24576]);

#endif

//
// File trailer for repmat.h
//
// [EOF]
//
