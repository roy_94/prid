//
// File: GOG_C1_data.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "GOG_C1_data.h"

// Variable Definitions
omp_nest_lock_t emlrtNestLockGlobal;

//
// File trailer for GOG_C1_data.cpp
//
// [EOF]
//
