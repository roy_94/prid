//
// File: xdotc.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "xdotc.h"

// Function Definitions

//
// Arguments    : int n
//                const double x[2116]
//                int ix0
//                const double y[2116]
//                int iy0
// Return Type  : double
//
double b_xdotc(int n, const double x[2116], int ix0, const double y[2116], int
               iy0)
{
  double d;
  int ix;
  int iy;
  int k;
  d = 0.0;
  ix = ix0;
  iy = iy0;
  for (k = 1; k <= n; k++) {
    d += x[ix - 1] * y[iy - 1];
    ix++;
    iy++;
  }

  return d;
}

//
// Arguments    : int n
//                const double x[81]
//                int ix0
//                const double y[81]
//                int iy0
// Return Type  : double
//
double xdotc(int n, const double x[81], int ix0, const double y[81], int iy0)
{
  double d;
  int ix;
  int iy;
  int k;
  d = 0.0;
  ix = ix0;
  iy = iy0;
  for (k = 1; k <= n; k++) {
    d += x[ix - 1] * y[iy - 1];
    ix++;
    iy++;
  }

  return d;
}

//
// File trailer for xdotc.cpp
//
// [EOF]
//
