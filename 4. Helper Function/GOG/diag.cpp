//
// File: diag.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "diag.h"

// Function Definitions

//
// Arguments    : const double v[2116]
//                double d[46]
// Return Type  : void
//
void diag(const double v[2116], double d[46])
{
  int j;
  for (j = 0; j < 46; j++) {
    d[j] = v[j * 47];
  }
}

//
// File trailer for diag.cpp
//
// [EOF]
//
