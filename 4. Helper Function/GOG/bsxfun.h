//
// File: bsxfun.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//
#ifndef BSXFUN_H
#define BSXFUN_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_defines.h"
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "omp.h"
#include "GOG_C1_types.h"

// Function Declarations
extern void b_bsxfun(const double a[288], const double b[48], double c[288]);
extern void bsxfun(const double a[768], const double b[128], double c[768]);
extern void c_bsxfun(const double a[2116], const double b[46], double c[2116]);

#endif

//
// File trailer for bsxfun.h
//
// [EOF]
//
