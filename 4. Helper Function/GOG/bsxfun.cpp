//
// File: bsxfun.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "bsxfun.h"

// Function Definitions

//
// Arguments    : const double a[288]
//                const double b[48]
//                double c[288]
// Return Type  : void
//
void b_bsxfun(const double a[288], const double b[48], double c[288])
{
  int ak;
  int ck;
  int k;
  ak = 0;
  for (ck = 0; ck <= 241; ck += 48) {
    for (k = 0; k < 48; k++) {
      c[ck + k] = a[ak + k] / b[k];
    }

    ak += 48;
  }
}

//
// Arguments    : const double a[768]
//                const double b[128]
//                double c[768]
// Return Type  : void
//
void bsxfun(const double a[768], const double b[128], double c[768])
{
  int ak;
  int ck;
  int k;
  ak = 0;
  for (ck = 0; ck <= 641; ck += 128) {
    for (k = 0; k < 128; k++) {
      c[ck + k] = a[ak + k] / b[k];
    }

    ak += 128;
  }
}

//
// Arguments    : const double a[2116]
//                const double b[46]
//                double c[2116]
// Return Type  : void
//
void c_bsxfun(const double a[2116], const double b[46], double c[2116])
{
  int ak;
  int bk;
  int ck;
  int k;
  ak = 0;
  bk = 0;
  for (ck = 0; ck <= 2071; ck += 46) {
    for (k = 0; k < 46; k++) {
      c[ck + k] = a[ak + k] * b[bk];
    }

    ak += 46;
    bk++;
  }
}

//
// File trailer for bsxfun.cpp
//
// [EOF]
//
