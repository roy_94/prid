//
// File: imlincomb.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "imlincomb.h"

// Function Definitions

//
// Arguments    : double varargin_1
//                const unsigned char varargin_2[6144]
//                double varargin_3
//                const unsigned char varargin_4[6144]
//                double varargin_5
//                const unsigned char varargin_6[6144]
//                double varargin_7
//                unsigned char Z[6144]
// Return Type  : void
//
void imlincomb(double varargin_1, const unsigned char varargin_2[6144], double
               varargin_3, const unsigned char varargin_4[6144], double
               varargin_5, const unsigned char varargin_6[6144], double
               varargin_7, unsigned char Z[6144])
{
  int k;
  double val;
  for (k = 0; k < 6144; k++) {
    val = ((varargin_1 * (double)varargin_2[k] + varargin_3 * (double)
            varargin_4[k]) + varargin_5 * (double)varargin_6[k]) + varargin_7;
    if (val > 255.0) {
      Z[k] = MAX_uint8_T;
    } else if (val < 0.0) {
      Z[k] = 0;
    } else if (val > 0.0) {
      Z[k] = (unsigned char)(val + 0.5);
    } else {
      Z[k] = 0;
    }
  }
}

//
// File trailer for imlincomb.cpp
//
// [EOF]
//
