//
// File: repmat.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "repmat.h"
#include "GOG_C1_emxutil.h"

// Function Definitions

//
// Arguments    : const double a_data[]
//                const int a_size[1]
//                emxArray_real_T *b
// Return Type  : void
//
void b_repmat(const double a_data[], const int a_size[1], emxArray_real_T *b)
{
  int nrows;
  int jtilecol;
  int ibtile;
  int k;
  nrows = b->size[0] * b->size[1];
  b->size[0] = (short)a_size[0];
  b->size[1] = 45;
  emxEnsureCapacity((emxArray__common *)b, nrows, (int)sizeof(double));
  if ((!(a_size[0] == 0)) && (!((short)a_size[0] == 0))) {
    nrows = a_size[0];
    for (jtilecol = 0; jtilecol < 45; jtilecol++) {
      ibtile = jtilecol * nrows;
      for (k = 1; k <= nrows; k++) {
        b->data[(ibtile + k) - 1] = a_data[k - 1];
      }
    }
  }
}

//
// Arguments    : const double a[45]
//                double varargin_1
//                emxArray_real_T *b
// Return Type  : void
//
void c_repmat(const double a[45], double varargin_1, emxArray_real_T *b)
{
  int jcol;
  int ibmat;
  int itilerow;
  jcol = b->size[0] * b->size[1];
  b->size[0] = (short)(int)varargin_1;
  b->size[1] = 45;
  emxEnsureCapacity((emxArray__common *)b, jcol, (int)sizeof(double));
  if (!((short)(int)varargin_1 == 0)) {
    for (jcol = 0; jcol < 45; jcol++) {
      ibmat = jcol * (int)varargin_1;
      for (itilerow = 1; itilerow <= (int)varargin_1; itilerow++) {
        b->data[(ibmat + itilerow) - 1] = a[jcol];
      }
    }
  }
}

//
// Arguments    : const double a_data[]
//                const int a_size[1]
//                emxArray_real_T *b
// Return Type  : void
//
void d_repmat(const double a_data[], const int a_size[1], emxArray_real_T *b)
{
  int nrows;
  int jtilecol;
  int ibtile;
  int k;
  nrows = b->size[0] * b->size[1];
  b->size[0] = (short)a_size[0];
  b->size[1] = 1035;
  emxEnsureCapacity((emxArray__common *)b, nrows, (int)sizeof(double));
  if ((!(a_size[0] == 0)) && (!((short)a_size[0] == 0))) {
    nrows = a_size[0];
    for (jtilecol = 0; jtilecol < 1035; jtilecol++) {
      ibtile = jtilecol * nrows;
      for (k = 1; k <= nrows; k++) {
        b->data[(ibtile + k) - 1] = a_data[k - 1];
      }
    }
  }
}

//
// Arguments    : const double a[6144]
//                double b[24576]
// Return Type  : void
//
void repmat(const double a[6144], double b[24576])
{
  int jtilecol;
  int ibtile;
  int jcol;
  int iacol;
  int ibmat;
  for (jtilecol = 0; jtilecol < 4; jtilecol++) {
    ibtile = jtilecol * 6144;
    for (jcol = 0; jcol < 48; jcol++) {
      iacol = jcol << 7;
      ibmat = ibtile + (jcol << 7);
      memcpy(&b[ibmat], &a[iacol], sizeof(double) << 7);
    }
  }
}

//
// File trailer for repmat.cpp
//
// [EOF]
//
