//
// File: eye.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "eye.h"

// Function Definitions

//
// Arguments    : double I[2025]
// Return Type  : void
//
void b_eye(double I[2025])
{
  int k;
  memset(&I[0], 0, 2025U * sizeof(double));
  for (k = 0; k < 45; k++) {
    I[k + 45 * k] = 1.0;
  }
}

//
// Arguments    : double I[64]
// Return Type  : void
//
void eye(double I[64])
{
  int k;
  memset(&I[0], 0, sizeof(double) << 6);
  for (k = 0; k < 8; k++) {
    I[k + (k << 3)] = 1.0;
  }
}

//
// File trailer for eye.cpp
//
// [EOF]
//
