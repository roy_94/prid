//
// File: xgetrf.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 09-Apr-2018 22:02:18
//

// Include Files
#include "rt_nonfinite.h"
#include "GOG_C1.h"
#include "xgetrf.h"
#include "xswap.h"

// Function Definitions

//
// Arguments    : double A[2025]
//                int ipiv[45]
// Return Type  : void
//
void xgetrf(double A[2025], int ipiv[45])
{
  int i18;
  int j;
  int c;
  int jA;
  int ix;
  double smax;
  int jy;
  double s;
  int b_j;
  int ijA;
  for (i18 = 0; i18 < 45; i18++) {
    ipiv[i18] = 1 + i18;
  }

  for (j = 0; j < 44; j++) {
    c = j * 46;
    jA = 1;
    ix = c;
    smax = fabs(A[c]);
    for (jy = 2; jy <= 45 - j; jy++) {
      ix++;
      s = fabs(A[ix]);
      if (s > smax) {
        jA = jy;
        smax = s;
      }
    }

    if (A[(c + jA) - 1] != 0.0) {
      if (jA - 1 != 0) {
        ipiv[j] = j + jA;
        b_xswap(A, j + 1, j + jA);
      }

      i18 = (c - j) + 45;
      for (jA = c + 1; jA + 1 <= i18; jA++) {
        A[jA] /= A[c];
      }
    }

    jA = c + 46;
    jy = c + 45;
    for (b_j = 1; b_j <= 44 - j; b_j++) {
      smax = A[jy];
      if (A[jy] != 0.0) {
        ix = c + 1;
        i18 = (jA - j) + 44;
        for (ijA = jA; ijA + 1 <= i18; ijA++) {
          A[ijA] += A[ix] * -smax;
          ix++;
        }
      }

      jy += 45;
      jA += 45;
    }
  }
}

//
// File trailer for xgetrf.cpp
//
// [EOF]
//
