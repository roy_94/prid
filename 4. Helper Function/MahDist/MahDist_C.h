
//
#ifndef MAHDIST_C_H
#define MAHDIST_C_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "MahDist_C_types.h"

// Function Declarations
extern double MahDist_C(double *Xg, double *Xp);

#endif

//
// File trailer for MahDist_C.h
//
// [EOF]
//
