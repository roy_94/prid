
//

// Include Files
#include "rt_nonfinite.h"
#include "MahDist_C.h"
#include "M.h"

// Function Definitions

//

//
double MahDist_C(double *Xg,  double *Xp)
{
  double b_Xg[249];
  double x[249];
  int k;
  double u;
  int i0;
  double v;
  double d0;
  for (k = 0; k < 249; k++) {
    b_Xg[k] = 0.0;
    for (i0 = 0; i0 < 249; i0++) {
      b_Xg[k] += (*(Xg+i0)) * M1[i0 + 249 * k];
    }

    x[k] = b_Xg[k] * (*(Xg + k));
  }

  u = x[0];
  for (k = 0; k < 248; k++) {
    u += x[k + 1];
  }

  for (k = 0; k < 249; k++) {
    b_Xg[k] = 0.0;
    for (i0 = 0; i0 < 249; i0++) {
      b_Xg[k] += (*(Xp + i0)) * M1[i0 + 249 * k];
    }

    x[k] = b_Xg[k] * (*(Xp + k));
  }

  v = x[0];
  for (k = 0; k < 248; k++) {
    v += x[k + 1];
  }

  d0 = 0.0;
  for (k = 0; k < 249; k++) {
    b_Xg[k] = 0.0;
    for (i0 = 0; i0 < 249; i0++) {
      b_Xg[k] += 2.0 * (*(Xg + i0)) * M1[i0 + 249 * k];
    }

    d0 += b_Xg[k] * (*(Xp + k));
  }

  return (u + v) - d0;
}

//////////////////////////  END   ///////////////////////////////////////////////
