//
// File: MahDist_C_terminate.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 31-Mar-2018 21:15:23
//

// Include Files
#include "rt_nonfinite.h"
#include "MahDist_C.h"
#include "MahDist_C_terminate.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void MahDist_C_terminate()
{
  // (no terminate code required)
}

//
// File trailer for MahDist_C_terminate.cpp
//
// [EOF]
//
