//
// File: MahDist_C_initialize.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 31-Mar-2018 21:15:23
//
#ifndef MAHDIST_C_INITIALIZE_H
#define MAHDIST_C_INITIALIZE_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "MahDist_C_types.h"

// Function Declarations
extern void MahDist_C_initialize();

#endif

//
// File trailer for MahDist_C_initialize.h
//
// [EOF]
//
