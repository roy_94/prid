//
// File: Projection_W_initialize.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 31-Mar-2018 11:12:35
//

// Include Files
#include "rt_nonfinite.h"
#include "Projection_W.h"
#include "Projection_W_initialize.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void Projection_W_initialize()
{
  rt_InitInfAndNaN(8U);
}

//
// File trailer for Projection_W_initialize.cpp
//
// [EOF]
//
