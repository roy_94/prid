

// Include Files
#include "rt_nonfinite.h"
#include "Projection_W.h"
#include "W.h"


#define FEATURE_SIZE 7567
#define M_SIZE 249

// Function Definitions


//
void Projection_W(double feature_vec[FEATURE_SIZE],
                  double Projected_feature_vec[M_SIZE])
{
  int i0;
  int i1;
  for (i0 = 0; i0 < M_SIZE; i0++) {
    Projected_feature_vec[i0] = 0.0;
    for (i1 = 0; i1 < FEATURE_SIZE; i1++) {
      Projected_feature_vec[i0] += feature_vec[i1] * W1[i1 + FEATURE_SIZE * i0];
    }
  }
}

//
// File trailer for Projection_W.cpp
//
// [EOF]
//
