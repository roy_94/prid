
//
#ifndef PROJECTION_W_H
#define PROJECTION_W_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "Projection_W_types.h"
#define FEATURE_SIZE 7567
#define M_SIZE 249

// Function Declarations
extern void Projection_W(double feature_vec[FEATURE_SIZE],
	double Projected_feature_vec[M_SIZE]);

#endif
