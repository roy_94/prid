//
// File: Projection_W_initialize.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 31-Mar-2018 11:12:35
//
#ifndef PROJECTION_W_INITIALIZE_H
#define PROJECTION_W_INITIALIZE_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "Projection_W_types.h"

// Function Declarations
extern void Projection_W_initialize();

#endif

//
// File trailer for Projection_W_initialize.h
//
// [EOF]
//
