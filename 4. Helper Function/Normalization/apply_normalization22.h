
//
#ifndef APPLY_NORMALIZATION22_H
#define APPLY_NORMALIZATION22_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "apply_normalization22_types.h"

// Function Declarations
extern void apply_normalization22(double X[7567], double Z[7567]);

#endif

//
// File trailer for apply_normalization22.h
//
// [EOF]
//
