
// Include Files
#include "rt_nonfinite.h"
#include "apply_normalization22.h"
#include "MEAN_H.h"



//
void apply_normalization22(double X[7567],  double Z[7567])
{
  double y;
  double scale;
  int jcol;
  double b_Z;
  double absxk;
  double t;

  //  Mean removal
  y = 0.0;
  scale = 2.2250738585072014E-308;
  for (jcol = 0; jcol < 7567; jcol++) {
    b_Z = X[jcol] - mean1[jcol];
    absxk = fabs(b_Z);
    if (absxk > scale) {
      t = scale / absxk;
      y = 1.0 + y * t * t;
      scale = absxk;
    } else {
      t = absxk / scale;
      y += t * t;
    }

    Z[jcol] = b_Z;
  }

  y = scale * sqrt(y);
  for (jcol = 0; jcol < 7567; jcol++) {
    Z[jcol] /= y;
  }

  //  L2 norm normalization
}

//
// File trailer for apply_normalization22.cpp
//
// [EOF]
//
