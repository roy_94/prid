

// Include Files
#include "rt_nonfinite.h"
#include "apply_normalization22.h"
#include "apply_normalization22_initialize.h"


//
void apply_normalization22_initialize()
{
  rt_InitInfAndNaN(8U);
}

//
// File trailer for apply_normalization22_initialize.cpp
//
// [EOF]
//
