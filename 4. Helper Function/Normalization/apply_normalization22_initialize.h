//
// File: apply_normalization22_initialize.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 09:56:55
//
#ifndef APPLY_NORMALIZATION22_INITIALIZE_H
#define APPLY_NORMALIZATION22_INITIALIZE_H

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "apply_normalization22_types.h"

// Function Declarations
extern void apply_normalization22_initialize();

#endif

//
// File trailer for apply_normalization22_initialize.h
//
// [EOF]
//
