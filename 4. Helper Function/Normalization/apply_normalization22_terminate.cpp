//
// File: apply_normalization22_terminate.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 09:56:55
//

// Include Files
#include "rt_nonfinite.h"
#include "apply_normalization22.h"
#include "apply_normalization22_terminate.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void apply_normalization22_terminate()
{
  // (no terminate code required)
}

//
// File trailer for apply_normalization22_terminate.cpp
//
// [EOF]
//
