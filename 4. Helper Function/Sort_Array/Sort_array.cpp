//
// File: Sort_array.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//

// Include Files
#include "rt_nonfinite.h"
#include "Sort_array.h"
#include "sort1.h"

// Function Definitions

//
// Arguments    : const double dist[316]
//                double Sorted_index[316]
// Return Type  : void
//
void Sort_array(const double dist[316], double Sorted_index[316])
{
  double unusedU0[316];
  int iidx[316];
  int i;
  memcpy(&unusedU0[0], &dist[0], 316U * sizeof(double));
  sort(unusedU0, iidx);
  for (i = 0; i < 316; i++) {
    Sorted_index[i] = iidx[i]-1;
  }

  //  rank the score
}

//
// File trailer for Sort_array.cpp
//
// [EOF]
//
