//
// File: sortIdx.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//
#ifndef SORTIDX_H
#define SORTIDX_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Sort_array_types.h"

// Function Declarations
extern void merge_block(int idx[316], double x[316], int offset, int n, int
  preSortLevel, int iwork[316], double xwork[316]);
extern void merge_pow2_block(int idx[316], double x[316], int offset);

#endif

//
// File trailer for sortIdx.h
//
// [EOF]
//
