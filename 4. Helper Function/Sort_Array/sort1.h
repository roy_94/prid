//
// File: sort1.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//
#ifndef SORT1_H
#define SORT1_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Sort_array_types.h"

// Function Declarations
extern void sort(double x[316], int idx[316]);

#endif

//
// File trailer for sort1.h
//
// [EOF]
//
