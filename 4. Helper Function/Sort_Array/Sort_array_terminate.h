//
// File: Sort_array_terminate.h
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//
#ifndef SORT_ARRAY_TERMINATE_H
#define SORT_ARRAY_TERMINATE_H

// Include Files
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "Sort_array_types.h"

// Function Declarations
extern void Sort_array_terminate();

#endif

//
// File trailer for Sort_array_terminate.h
//
// [EOF]
//
