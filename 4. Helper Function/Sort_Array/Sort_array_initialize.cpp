//
// File: Sort_array_initialize.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//

// Include Files
#include "rt_nonfinite.h"
#include "Sort_array.h"
#include "Sort_array_initialize.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void Sort_array_initialize()
{
  rt_InitInfAndNaN(8U);
}

//
// File trailer for Sort_array_initialize.cpp
//
// [EOF]
//
