//
// File: sort1.cpp
//
// MATLAB Coder version            : 3.1
// C/C++ source code generated on  : 01-Apr-2018 11:02:50
//

// Include Files
#include "rt_nonfinite.h"
#include "Sort_array.h"
#include "sort1.h"
#include "sortIdx.h"

// Function Declarations
static void b_sort(double x[316], int idx[316]);

// Function Definitions

//
// Arguments    : double x[316]
//                int idx[316]
// Return Type  : void
//
static void b_sort(double x[316], int idx[316])
{
  double x4[4];
  short idx4[4];
  int m;
  double xwork[316];
  int nNaNs;
  int ib;
  int k;
  signed char perm[4];
  int iwork[316];
  int i2;
  int i3;
  int i4;
  memset(&idx[0], 0, 316U * sizeof(int));
  for (m = 0; m < 4; m++) {
    x4[m] = 0.0;
    idx4[m] = 0;
  }

  memset(&xwork[0], 0, 316U * sizeof(double));
  nNaNs = -315;
  ib = 0;
  for (k = 0; k < 316; k++) {
    if (rtIsNaN(x[k])) {
      idx[-nNaNs] = k + 1;
      xwork[-nNaNs] = x[k];
      nNaNs++;
    } else {
      ib++;
      idx4[ib - 1] = (short)(k + 1);
      x4[ib - 1] = x[k];
      if (ib == 4) {
        ib = (k - nNaNs) - 318;
        if (x4[0] <= x4[1]) {
          m = 1;
          i2 = 2;
        } else {
          m = 2;
          i2 = 1;
        }

        if (x4[2] <= x4[3]) {
          i3 = 3;
          i4 = 4;
        } else {
          i3 = 4;
          i4 = 3;
        }

        if (x4[m - 1] <= x4[i3 - 1]) {
          if (x4[i2 - 1] <= x4[i3 - 1]) {
            perm[0] = (signed char)m;
            perm[1] = (signed char)i2;
            perm[2] = (signed char)i3;
            perm[3] = (signed char)i4;
          } else if (x4[i2 - 1] <= x4[i4 - 1]) {
            perm[0] = (signed char)m;
            perm[1] = (signed char)i3;
            perm[2] = (signed char)i2;
            perm[3] = (signed char)i4;
          } else {
            perm[0] = (signed char)m;
            perm[1] = (signed char)i3;
            perm[2] = (signed char)i4;
            perm[3] = (signed char)i2;
          }
        } else if (x4[m - 1] <= x4[i4 - 1]) {
          if (x4[i2 - 1] <= x4[i4 - 1]) {
            perm[0] = (signed char)i3;
            perm[1] = (signed char)m;
            perm[2] = (signed char)i2;
            perm[3] = (signed char)i4;
          } else {
            perm[0] = (signed char)i3;
            perm[1] = (signed char)m;
            perm[2] = (signed char)i4;
            perm[3] = (signed char)i2;
          }
        } else {
          perm[0] = (signed char)i3;
          perm[1] = (signed char)i4;
          perm[2] = (signed char)m;
          perm[3] = (signed char)i2;
        }

        idx[ib] = idx4[perm[0] - 1];
        idx[ib + 1] = idx4[perm[1] - 1];
        idx[ib + 2] = idx4[perm[2] - 1];
        idx[ib + 3] = idx4[perm[3] - 1];
        x[ib] = x4[perm[0] - 1];
        x[ib + 1] = x4[perm[1] - 1];
        x[ib + 2] = x4[perm[2] - 1];
        x[ib + 3] = x4[perm[3] - 1];
        ib = 0;
      }
    }
  }

  if (ib > 0) {
    for (m = 0; m < 4; m++) {
      perm[m] = 0;
    }

    if (ib == 1) {
      perm[0] = 1;
    } else if (ib == 2) {
      if (x4[0] <= x4[1]) {
        perm[0] = 1;
        perm[1] = 2;
      } else {
        perm[0] = 2;
        perm[1] = 1;
      }
    } else if (x4[0] <= x4[1]) {
      if (x4[1] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 2;
        perm[2] = 3;
      } else if (x4[0] <= x4[2]) {
        perm[0] = 1;
        perm[1] = 3;
        perm[2] = 2;
      } else {
        perm[0] = 3;
        perm[1] = 1;
        perm[2] = 2;
      }
    } else if (x4[0] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 1;
      perm[2] = 3;
    } else if (x4[1] <= x4[2]) {
      perm[0] = 2;
      perm[1] = 3;
      perm[2] = 1;
    } else {
      perm[0] = 3;
      perm[1] = 2;
      perm[2] = 1;
    }

    for (k = 1; k <= ib; k++) {
      idx[(k - nNaNs) - ib] = idx4[perm[k - 1] - 1];
      x[(k - nNaNs) - ib] = x4[perm[k - 1] - 1];
    }
  }

  m = (nNaNs + 315) >> 1;
  for (k = 1; k <= m; k++) {
    ib = idx[k - nNaNs];
    idx[k - nNaNs] = idx[316 - k];
    idx[316 - k] = ib;
    x[k - nNaNs] = xwork[316 - k];
    x[316 - k] = xwork[k - nNaNs];
  }

  if (((nNaNs + 315) & 1) != 0) {
    x[(m - nNaNs) + 1] = xwork[(m - nNaNs) + 1];
  }

  memset(&iwork[0], 0, 316U * sizeof(int));
  m = 2;
  if (1 - nNaNs > 1) {
    ib = (1 - nNaNs) >> 8;
    if (ib > 0) {
      for (m = 1; m <= ib; m++) {
        merge_pow2_block(idx, x, (m - 1) << 8);
      }

      m = ib << 8;
      ib = 1 - (nNaNs + m);
      if (ib > 0) {
        merge_block(idx, x, m, ib, 2, iwork, xwork);
      }

      m = 8;
    }

    merge_block(idx, x, 0, 1 - nNaNs, m, iwork, xwork);
  }
}

//
// Arguments    : double x[316]
//                int idx[316]
// Return Type  : void
//
void sort(double x[316], int idx[316])
{
  b_sort(x, idx);
}

//
// File trailer for sort1.cpp
//
// [EOF]
//
